<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {

        // Member Roles Seeder
        DB::table('roles')->insert([
            'role_name' => "Admin",
            'role_description' => "Budget Administrator",
        ]);

        DB::table('roles')->insert([
            'role_name' => "Member",
            'role_description' => "Budget Member",
        ]);
    }
}
