<?php

namespace App\Http\Controllers;

use App\Veto;
use App\Proposal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VetoController extends Controller
{
    public function postCreateveto(Request $request)
    {
        // Validate fields.
        $this->validate($request, [
            'proposal_id' => 'required|integer',
            'veto_reason' => 'required|max:250',
            'veto_action' => 'required',
        ]);

        //Check if allocation method is valid.
        $veto_action = $request['veto_action'];
        if (!($veto_action == 'create' || $veto_action == 'edit' || $veto_action == 'delete'))
        {
            // Error on invalid post
            return redirect()->back()->withInput()->withErrors(
                ['invalid_veto' => 'The veto is invalid.']
            );
        }

        $member_id = Auth::user()->members()->where('budget_id', '=', $request['budget_id'])->first()->id;

        //If veto exists.
        $proposal = Proposal::find($request['proposal_id']);
        $veto = $proposal->vetos()->where('member_id', '=', $member_id)->first();

        // If the veto exists.
        if($veto != null)
        {
            if($request['veto_action'] == 'delete')
            {
                $veto = Veto::find($veto->id);
                $veto->delete();

                flash('You have successfully removed your veto', 'success');
                return redirect()->back();
            }

            $veto = Veto::find($veto->id);
            $veto->veto_reason = $request['veto_reason'];
            $veto->save();

            flash('You have successfully edited your veto.', 'success');
            return redirect()->back();
        }

        // Make and Save new priority
        $veto = new Veto();
        $veto->proposal_id = $request['proposal_id'];
        $veto->member_id = $member_id;
        $veto->veto_reason = $request['veto_reason'];
        $veto->save();

        // Redirect to same page.
        flash('You have successfully vetod this proposal.', 'success');
        return redirect()->back();
    }
}
