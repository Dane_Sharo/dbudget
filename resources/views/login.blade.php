@extends('master')

@section('content')
<div class="hidden">{{$page_title = 'Sign In | Sign Up' }}</div>
<div class='row'>

    <div class='col-md-6'>

        <!-- Box -->
        <a name="sign_in"></a>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Sign In</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box-body">
                <p class="login-box-msg">Sign in to start your session</p>

                <form action="{{ route('user/signin') }}" method="post">
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="Email" name="email" id="email" value="{{Request::old('email')}}">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Password" name="password" id="password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-xs-4 pull-right">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                            <input type="hidden" name="_token" value="{{Session::token()}}">
                        </div>
                        <!-- /.col -->
                    </div>
                </form>

            </div>
            <!-- /.login-box-body -->

        </div><!-- /.box -->


    </div><!-- /.col -->
    <div class='col-md-6'>

        <!-- Box -->
        <a  name="sign_up"></a>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Sign Up</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>

            <div class="box-body">
                <p class="login-box-msg">Register a new membership</p>

                <form action="{{ route('user/signup') }}" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Full name" name="name" id="name" value="{{Request::old('name')}}">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" placeholder="Email" name="email" id="email" value="{{Request::old('email')}}">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Password" name="password" id="password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Retype password" name="password_confirmation" id="password_confirmation">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-xs-4 pull-right">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                            <input type="hidden" name="_token" value="{{Session::token()}}">
                        </div>
                        <!-- /.col -->
                    </div>
                </form>

            </div>
            <!-- /.form-box -->

        </div><!-- /.box -->


    </div><!-- /.col -->
</div><!-- /.row -->

@stop


@section('javascript')

<script>
    //Insert javascript here
</script>

@stop
