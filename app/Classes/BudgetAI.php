<?php

namespace App\Classes;

use App\Budget;
use App\Priority;
use App\Proposal;

class BudgetAI
{
    public static function generateBudget($budget)
    {
        // Set temporary funds.
        $budget->temp_funds = $budget->budget_funds;

        // Get priorities
        $priorities = collect(new Priority);
        $priorities = $budget->priorities()->get();

        // Get all proposals within each priority
        $proposals = collect(new Proposal);
        foreach ($priorities as $priority)
        {
            $priority->available_funds = $budget->budget_funds * ($priority->max_percentage / 100);
            $proposals = $proposals->merge($priority->proposals()->get());
        }

        // Sort proposals by votes in decending value and cost in ascending.
        $proposals = $proposals->sortBy(function($proposal) {
            // Creates formatted string for closure.
            return sprintf('%-12s%s', $proposal->votes()->count(), ( 1 / $proposal->proposal_cost));
        })->reverse();

        // Proposal Selection
        $finalproposals = collect(new Proposal);
        $veto_count = 0;
        $proposal_count = 0;
        foreach ($proposals as $proposal)
        {
            $passed = true;
            $proposal_count += 1;
            $priority = $priorities->find($proposal->priority_id);

            // Skip if no agree votes.
            if($proposal->votes()->where('user_vote', '=' , 'agree')->count() == 0)
            {
                    $passed = false;
            }

            // Skip if agree votes is less than 1.
            if($proposal->votes()->where('user_vote', '=' , 'agree')->count() > 0)
            {
                // Failed if agree votes is less than budget min agree percent
                $proposal_agree = $proposal->votes()->where('user_vote', '=' , 'agree')->count();
                $proposal_disagree = $proposal->votes()->where('user_vote', '=' , 'disagree')->count();
                $proposal_agreefraction = $proposal_agree / ($proposal_agree + $proposal_disagree);
                if($proposal_agreefraction < $budget->min_agree_percent / 100)
                {
                    $passed = false;
                }
            }

            // Failed if has any vetos...
            if($proposal->vetos()->count() > 0)
            {
                $veto_count += 1;
                $passed = false;
            }

            // Failed if proposal cost is greater than available priority funds.
            if($proposal->proposal_cost > $priority->available_funds)
            {
                $passed = false;
            }

            // Failed if proposal cost is greater than available budget funds.
            if($proposal->proposal_cost > $budget->temp_funds)
            {
                $passed = false;
            }

            // Failed if votes are less than min votes.
            if($proposal->votes()->count() < $budget->min_vote)
            {
                $passed = false;
            }

            // If passed, update proposal cost and add to finalproposals collection.
            if ($passed === true)
            {
                $priority->available_funds = $priority->available_funds - $proposal->proposal_cost;
                $budget->temp_funds = $budget->temp_funds - $proposal->proposal_cost;
                $finalproposals = $finalproposals->push($proposal);
            }
        }

        return [
            'finalproposals' => $finalproposals,
            'unused_funds' => $budget->temp_funds,
            'veto_count' => $veto_count,
            'proposal_count' => $proposal_count,
        ];

    }

}
