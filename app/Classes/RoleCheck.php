<?php

namespace App\Classes;

class RoleCheck
{
    public static function isAdmin($budget, $user)
    {
        // Setup output
        $budget_id = $budget->id;
        // Get member record in budget based in current user request...
        $member = $user->members()->where('budget_id', '=', $budget_id)->first();

        if($member === null)
        {
            return false;
        }

        // Return true if admin...
        if($member->role()->first()->role_name === 'Admin')
        {
            return true;
        }
        return false;
    }
}
