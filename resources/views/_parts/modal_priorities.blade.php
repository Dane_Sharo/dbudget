
<!-- Modal -->
<div class="modal fade" id="addPriorityModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add Priority</h4>
            </div>

            <!-- Modal Body -->
            <form action="{{ route('priority/create') }}" method="post">
                <div class="modal-body">

                    <input type="hidden" name="budget_id" value="{{ $budget->id }}">

                    <div class="form-group">
                        <label for="PriorityName">Priority Name</label>
                        <input type="text" class="form-control" name="priority_name" id="priority_name" placeholder="Priority Name" value="{{Request::old('priority_name')}}">
                    </div>

                    <div class="form-group">
                        <label for="PriorityName">Priority Description</label>
                        <textarea class="form-control" rows="3" placeholder="Description of this priority" name="priority_description" id="priority_description">{{Request::old('priority_description')}}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="PriorityName">Max Budget Percentage</label>
                        <input type="number" class="form-control" name="max_percentage" id="max_percentage" placeholder="%" value="{{Request::old('max_percentage')}}">
                    </div>

                </div>
                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <input type="hidden" name="_token" value="{{Session::token()}}">
                </div>
            </form>
        </div>
    </div>
</div>
