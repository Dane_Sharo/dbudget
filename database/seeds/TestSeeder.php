<?php

use App\User;
use App\Budget;
use App\Priority;
use App\Member;
use App\Proposal;
use App\Vote;
use App\Comment;
use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

use Faker\Factory as Faker;

class TestSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        // Use "php artisan db:seed --class=TestSeeder" to run seeder.

        $faker = Faker::create();
        foreach (range(1,15) as $index) {
            User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => bcrypt('secret'),
            ]);
        }

        $users = User::all()->pluck('id')->toArray();
        foreach(range(1,5) as $index){
            Budget::create([
                'user_id' => $faker->randomElement($users),
                'budget_slug' => $faker->slug,
                'budget_name' => $faker->randomElement($array = array ('Football 2017', 'Welcome Party', 'Camping', 'Trip to Melaka', 'Karaoke Night', 'Badminton', 'Science Exhibit')),
                'budget_description' => $faker->text($maxNbChars = 200),
                'budget_funds' => $faker->numberBetween($min = 1000, $max = 5000),
                'budget_password' => '123456',
                'min_vote' => $faker->numberBetween($min = 1, $max = 5),
                'min_agree_percent' => $faker->numberBetween($min = 50, $max = 75),
                'proposal_deadline' => $faker->dateTimeBetween('now', '+1 months'),
                'vote_deadline' => $faker->dateTimeBetween('+1 months', '+2 months'),
            ]);
        }

        $budgets = Budget::all()->pluck('id')->toArray();
        foreach(range(1,25) as $index){
            $budget_id = $faker->randomElement($budgets);
            while (Budget::find($budget_id)->priorities()->count() > 4){
                $budget_id = $faker->randomElement($budgets);
            }
            Priority::create([
                'budget_id' => $budget_id,
                'priority_slug' => $faker->slug,
                'priority_name' => $faker->randomElement($array = array ('Food', 'Drinks', 'Equipment', 'Fun and Games', 'Other Expenditures', 'Snacks', 'Other Supplies', 'Admin Expenses')),
                'priority_description' => $faker->text($maxNbChars = 200),
                'max_percentage' => $faker->numberBetween($min = 30, $max = 50),
            ]);
        }

        // Make membership for budget owners...
        foreach($budgets as $budget)
        {
            $ownerid = Budget::find($budget)->user_id;
            Member::create([
                'budget_id' => $budget,
                'user_id' => $ownerid,
                'role_id' => 1,
            ]);
        }

        $roles = Role::all()->pluck('id')->toArray();
        foreach(range(1,50) as $index){
            $budget_id = $faker->randomElement($budgets);
            $user_id = $faker->randomElement($users);
            while (Member::where('budget_id', '=', $budget_id)->where('user_id', '=', $user_id)->count() > 0)
            {
                $budget_id = $faker->randomElement($budgets);
                $user_id = $faker->randomElement($users);
            }
            Member::create([
                'budget_id' => $budget_id,
                'user_id' => $user_id,
                'role_id' => $faker->randomElement($roles),
            ]);
        }



        $priorities = Priority::all()->pluck('id')->toArray();
        $members = Member::all()->pluck('id')->toArray();
        foreach(range(1,100) as $index){
            $priority_id = $faker->randomElement($priorities);
            $member_id = $faker->randomElement($members);
            $priority_budget_id = Priority::find($priority_id)->budget()->first()->id;
            while (Member::find($member_id)->budget_id != $budget_id)
            {
                $priority_id = $faker->randomElement($priorities);
                $member_id = $faker->randomElement($members);
                $budget_id = Priority::find($priority_id)->budget()->first()->id;
            }
            Proposal::create([
                'priority_id' => $priority_id,
                'member_id' => $member_id,
                'proposal_slug' => $faker->slug,
                'proposal_name' => $faker->sentence($nbWords = 2, $variableNbWords = true),
                'proposal_description' => $faker->text($maxNbChars = 200),
                'proposal_cost' => $faker->numberBetween($min = 100, $max = 500),
            ]);
        }

        $proposals = Proposal::all()->pluck('id')->toArray();
        foreach(range(1,500) as $index){
            $proposal_id = $faker->randomElement($proposals);
            $member_id = $faker->randomElement($members);
            $priority_id = Proposal::find($proposal_id)->priority()->first()->id;
            $budget_id = Priority::find($priority_id)->budget()->first()->id;
            while (Member::find($member_id)->budget_id != $budget_id)
            {
                $proposal_id = $faker->randomElement($proposals);
                $member_id = $faker->randomElement($members);
                $priority_id = Proposal::find($proposal_id)->priority()->first()->id;
                $budget_id = Priority::find($priority_id)->budget()->first()->id;
            }
            Vote::create([
                'proposal_id' => $proposal_id,
                'member_id' => $member_id,
                'user_vote' => $faker->randomElement(array('agree','agree','disagree')),
            ]);
        }

        foreach(range(1,500) as $index){
            $proposal_id = $faker->randomElement($proposals);
            $member_id = $faker->randomElement($members);
            $priority_id = Proposal::find($proposal_id)->priority()->first()->id;
            $budget_id = Priority::find($priority_id)->budget()->first()->id;
            while (Member::find($member_id)->budget_id != $budget_id)
            {
                $proposal_id = $faker->randomElement($proposals);
                $member_id = $faker->randomElement($members);
                $priority_id = Proposal::find($proposal_id)->priority()->first()->id;
                $budget_id = Priority::find($priority_id)->budget()->first()->id;
            }
            Comment::create([
                'proposal_id' => $faker->randomElement($proposals),
                'member_id' => $faker->randomElement($members),
                'user_comment' => $faker->text($maxNbChars = 200),
            ]);
        }

    }
}
