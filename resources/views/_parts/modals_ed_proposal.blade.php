
<!-- Modal -->
<div class="modal fade" id="editProposalModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Edit Proposal</h4>
            </div>

            <!-- Modal Body -->
            <form action="{{ route('proposal/edit') }}" method="post">
                <div class="modal-body">

                    <input type="hidden" name="budget_id" value="{{ $budget->id }}">
                    <input type="hidden" name="proposal_id" value="{{ $proposal->id }}">

                    <div class="form-group">
                        <label for="Priority">Priority</label>
                        <select class="form-control" name="priority_id">
                            @foreach($priorities as $priority_l)
                                @if($priority->id == $priority_l->id)
                                    <option value="{{ $priority_l->id }}" selected="selected">{{ $priority_l->priority_name }}</option>
                                @else
                                    <option value="{{ $priority_l->id }}">{{ $priority_l->priority_name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="ProposalName">Proposal Name</label>
                        <input type="text" class="form-control" name="proposal_name" id="proposal_name" placeholder="Proposal Name" value="{{ $proposal->proposal_name }}">
                    </div>

                    <div class="form-group">
                        <label for="ProposalName">Proposal Description</label>
                        <textarea class="form-control" rows="3" placeholder="Description of this proposal" name="proposal_description" id="proposal_description">{{ $proposal->proposal_description }}</textarea>
                    </div>

                    <!-- Proposal Cost - numeric -->
                    <div class="form-group">
                        <label>Proposal Cost</label>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="number" class="form-control" name="proposal_cost" id="proposal_cost" value="{{ $proposal->proposal_cost }}">
                        </div>
                    </div>

                </div>
                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <input type="hidden" name="_token" value="{{Session::token()}}">
                </div>
            </form>
        </div>
    </div>
</div>
