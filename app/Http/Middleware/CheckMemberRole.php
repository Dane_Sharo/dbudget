<?php

namespace App\Http\Middleware;

use Closure;
use App\Budget;
use App\Member;
use App\Role;

class CheckMemberRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // If not logged in...
        if ($request->user() === null)
        {
            // Redirect to welcome page...
            return redirect()->route('/')->withInput()->withErrors(
                ['permission' => 'You must log in with permission to do that.']
            );
        }

        // Get budget...
        $budget_slug = $request->route('budget_slug');
        $budget = Budget::where('budget_slug', '=', $budget_slug)->first();
        //If slug is not present but 'budget_id is available'
        if ($budget === null)
        {
            $budget = Budget::where('id', '=', $request['budget_id'])->first();
        }

        //If budget does not exist...
        if ($budget === null)
        {
            return abort(404);
        }

        $budget_id = $budget->id;
        // Get member record in budget based in current user request...
        $member = $request->user()->members()->where('budget_id', '=', $budget_id)->first();

        // If requested budget does not have a member record of current user request
        if ($member === null)
        {
            // Insufficient permission.
            return redirect()->back()->withInput()->withErrors(
                ['permission' => 'You do not have permission to do that.']
            );
        }

        // Get actions and roles specified in routes...
        $actions = $request->route()->getAction();
        $roles = isset($actions['roles']) ? $actions['roles'] : null ;
        // Compare requested role with member role....
        // If mamber role is valid continue to next request...
        if (is_array($roles))
        {
            foreach($roles as $role)
            {
                if($role === $member->role()->first()->role_name)
                {
                    return $next($request);
                }
            }
        }
        else
        {
            if($roles === $member->role()->first()->role_name)
            {
                return $next($request);
            }
        }

        // Insufficient permission.
        return redirect()->back()->withInput()->withErrors(
            ['permission' => 'You do not have permission to do that.']
        );
    }
}
