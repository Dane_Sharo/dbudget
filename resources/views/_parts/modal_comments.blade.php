
<!-- Modal -->
<div class="modal fade" id="addCommentModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add Comment</h4>
            </div>

            <!-- Modal Body -->
            <form action="{{ route('comment/create') }}" method="post">
                <div class="modal-body">

                    <input type="hidden" name="budget_id" value="{{ $budget->id }}">
                    <input type="hidden" name="priority_id" value="{{ $priority->id }}">
                    <input type="hidden" name="proposal_id" value="{{ $proposal->id }}">

                    <div class="form-group">
                        <label for="ProposalName">Your Comment</label>
                        <textarea class="form-control" rows="3" placeholder="Your comment on this proposal" name="user_comment" id="user_comment">{{Request::old('user_comment')}}</textarea>
                    </div>


                </div>
                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <input type="hidden" name="_token" value="{{Session::token()}}">
                </div>
            </form>
        </div>
    </div>
</div>
