<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    public function priority()
    {
        return $this->belongsTo('App\Priority', 'priority_id');
    }

    public function member()
    {
        return $this->belongsTo('App\Member', 'member_id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function votes()
    {
        return $this->hasMany('App\Vote');
    }

    public function vetos()
    {
        return $this->hasMany('App\Veto');
    }
}
