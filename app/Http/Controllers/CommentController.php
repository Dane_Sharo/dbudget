<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function postCreateComment(Request $request)
    {
        // Validate fields.
        $this->validate($request, [
            'proposal_id' => 'required|integer',
            'user_comment' => 'required|max:5000',
        ]);

        $member_id = Auth::user()->members()->where('budget_id', '=', $request['budget_id'])->first()->id;

        // Make and Save new priority
        $comment = new Comment();
        $comment->proposal_id = $request['proposal_id'];
        $comment->member_id = $member_id;
        $comment->user_comment = clean( $request['user_comment'] );
        $comment->save();

        // Redirect to same page.
        return redirect()->back();
    }
}
