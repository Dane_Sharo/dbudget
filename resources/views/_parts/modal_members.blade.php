
<!-- Modal -->
<div class="modal fade" id="addMembersModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Join Budget</h4>
            </div>

            <!-- Modal Body -->
            <form action="{{ route('member/create') }}" method="post">
            <div class="modal-body">

                <div class="form-group has-feedback">
                    <label for="BudgetSlug">Budget Key</label>
                    <input type="text" class="form-control" placeholder="Budget Key" name="budget_slug" id="budget_slug" value="{{Request::old('budget_slug')}}">
                </div>
                <div class="form-group has-feedback">
                    <label for="BudgetPassword">Budget Password</label>
                    <input type="password" class="form-control" placeholder="Password" name="password" id="password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>

            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Submit</button>
                <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                <input type="hidden" name="_token" value="{{Session::token()}}">
            </div>
            </form>
        </div>
    </div>
</div>
