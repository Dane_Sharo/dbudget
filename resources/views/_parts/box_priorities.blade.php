@include('_parts.modal_priorities')

<!-- Box -->
<a name="budget_priorities"></a>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-tags"></i>
        <h3 class="box-title">Spending Priorities</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>

    <div class="box-body">
        <div class="row">

            @foreach($priorities as $priority)
                <div class="col-md-12">
                    <div class="clickable info-box bg-blue">
                        <a href="{{ url('budget/' . $budget->budget_slug . '/priority/' . $priority->priority_slug)}}" class="small-box-footer"></a>
                        <span class="info-box-icon"><i class="fa fa-tag"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number uppercase pull-right"><i class="fa fa-fw fa-lightbulb-o"></i><b>{{$priority->proposals()->count()}}</b></span>
                            <span class="info-box-number uppercase truncate"><b>{{$priority->priority_name}}</b></span>
                            <span class="info-box-text">{{strip_tags($priority->priority_description)}}</span>
                            <div class="progress">
                                <div class="progress-bar" style="width: {{$priority->max_percentage}}%"></div>
                            </div>
                            <span class="progress-description">
                                {{$priority->max_percentage}}% Budget Allocated
                            </span>
                        </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->
                </div><!-- /.col -->
            @endforeach

        </div><!-- /.row -->
    </div><!-- /.priority-body -->

    <div class="box-footer clearfix">
        @if($isadmin)
            <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#addPriorityModal">
                <i class="fa fa-plus"></i> Add Priorities
            </button>
        @endif
    </div>

</div><!-- /.box -->
