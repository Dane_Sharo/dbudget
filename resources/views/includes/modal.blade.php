
<!-- Modal Form -->
<div class="modal fade" id="default_modal" tabindex="-1" role="dialog" aria-labelledby="favoritesModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">{{ $data['modal_title'] or 'Empty Modal' }}</h4>
            </div>
            <div class="modal-body">
                <p>{{ $data['modal_message'] or 'Modal Message...' }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Confirm</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
