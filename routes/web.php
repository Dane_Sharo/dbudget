<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/******************************************/
/*  Get Routes (For serving links/data)   */
/******************************************/

Route::get('/', function () {
    return view('welcome');
})->name('/');

Route::get('login', function () {
    return view('login');
})->name('login');

/* Send Logout Request */
Route::get('user/logout', [
    'uses'=>'UserController@getLogout',
    'as'=>'user/logout'
]);

/* Get Help */
Route::get('help', 'HelpController@index');

/* Get Home Page (Dashboard) */
Route::get('home', [
    'uses'=>'UserController@getDashboard',
    'as'=>'home',
    'middleware' => 'auth'
]);

/* Get Home Page (Dashboard) */
Route::get('profile', [
    'uses'=>'UserController@getProfile',
    'as'=>'profile',
    'middleware' => 'auth'
]);

/* Get Budget Creation Page */
Route::get('budget/create', [
    'uses' => 'BudgetController@getCreateBudget',
    'as' => 'budget/create',
    'middleware' => 'auth'
]);

/* Get Budget Join Page */
Route::get('budget/{budget_slug}/join', [
    'uses' => 'MemberController@getBudgetJoin',
    'as' => 'budget/{budget_slug}/join',
    'middleware' => 'auth'
]);

/* Get Budget Page */
Route::get('budget/{budget_slug}', [
    'uses' => 'BudgetController@getBudget',
    'as' => 'budget/{budget_slug}',
    'middleware' => ['auth', 'roles'],
    'roles' => ['Admin', 'Member']
]);

/* Get Budget Members Page */
Route::get('budget/{budget_slug}/members', [
    'uses' => 'MemberController@getBudgetMembers',
    'as' => 'budget/{budget_slug}/members',
    'middleware' => ['auth', 'roles'],
    'roles' => ['Admin', 'Member']
]);

/* Get Budget Page */
Route::get('budget/{budget_slug}/report', [
    'uses' => 'BudgetController@getBudgetReport',
    'as' => 'budget/{budget_slug}/report',
    'middleware' => ['auth', 'roles'],
    'roles' => ['Admin', 'Member']
]);

/* Get Priority Page */
Route::get('budget/{budget_slug}/priority/{priority_slug}', [
    'uses' => 'PriorityController@getPriority',
    'as' => 'budget/{budget_slug}/priority/{priority_slug}',
    'middleware' => ['auth', 'roles'],
    'roles' => ['Admin', 'Member']
]);

/* Get Proposal Page */
Route::get('budget/{budget_slug}/priority/{priority_slug}/proposal/{proposal_slug}', [
    'uses' => 'ProposalController@getProposal',
    'as' => 'budget/{budget_slug}/priority/{priority_slug}/proposal/{proposal_slug}',
    'middleware' => ['auth', 'roles'],
    'roles' => ['Admin', 'Member']
]);

/******************************************/
/*  Post Routes (For submitting forms)    */
/*  Use "controller_name/action"          */
/******************************************/

/* SignUp Processing */
Route::post('user/signin', [
    'uses'=>'UserController@postSignIn',
    'as'=>'user/signin'
]);

/* SignIn Processing */
Route::post('user/signup', [
    'uses'=>'UserController@postSignUp',
    'as'=>'user/signup'
]);

/* Edit Profile */
Route::post('user/edit_profile', [
    'uses'=>'UserController@postEditProfile',
    'as'=>'user/edit_profile'
]);

/* Edit Password */
Route::post('user/change_password', [
    'uses'=>'UserController@postChangePassword',
    'as'=>'user/change_password'
]);

/******************************************/
/*  Add Routes                            */
/*  For submitting new records            */
/******************************************/

/* Create Budget */
Route::post('budget/create', [
    'uses' => 'BudgetController@postCreateBudget',
    'as' => 'budget/create',
    'middleware' => 'auth'
]);

/* Add Member */
Route::post('member/create', [
    'uses' => 'MemberController@postCreateMember',
    'as' => 'member/create',
    'middleware' => 'auth'
]);

/* Invite Member */
Route::post('member/invite', [
    'uses' => 'MemberController@postInviteMember',
    'as' => 'member/invite',
    'middleware' => ['auth', 'roles'],
    'roles' => ['Admin']
]);

/* Add Priority */
Route::post('priority/create', [
    'uses' => 'PriorityController@postCreatePriority',
    'as' => 'priority/create',
    'middleware' => ['auth', 'roles'],
    'roles' => ['Admin']
]);

/* Add Proposal */
Route::post('proposal/create', [
    'uses' => 'ProposalController@postCreateProposal',
    'as' => 'proposal/create',
    'middleware' => ['auth', 'roles'],
    'roles' => ['Admin', 'Member']
]);

/* Add Comment */
Route::post('comment/create', [
    'uses' => 'CommentController@postCreateComment',
    'as' => 'comment/create',
    'middleware' => ['auth', 'roles'],
    'roles' => ['Admin', 'Member']
]);

/* Post Vote */
Route::post('vote/create', [
    'uses' => 'VoteController@postCreateVote',
    'as' => 'vote/create',
    'middleware' => ['auth', 'roles'],
    'roles' => ['Admin', 'Member']
]);

/* Post Vote */
Route::post('veto/create', [
    'uses' => 'VetoController@postCreateVeto',
    'as' => 'veto/create',
    'middleware' => ['auth', 'roles'],
    'roles' => ['Admin']
]);

/******************************************/
/*  Edit Routes                           */
/*  For submitting record edits           */
/******************************************/

/* Edit Budget */
Route::post('budget/edit', [
    'uses' => 'BudgetController@postEditBudget',
    'as' => 'budget/edit',
    'middleware' => ['auth', 'roles'],
    'roles' => ['Admin']
]);

/* Edit Budget */
Route::post('member/edit', [
    'uses' => 'MemberController@postEditMember',
    'as' => 'member/edit',
    'middleware' => ['auth', 'roles'],
    'roles' => ['Admin']
]);

/* Edit Priority */
Route::post('priority/edit', [
    'uses' => 'PriorityController@postEditPriority',
    'as' => 'priority/edit',
    'middleware' => ['auth', 'roles'],
    'roles' => ['Admin']
]);

/* Edit Proposal */
Route::post('proposal/edit', [
    'uses' => 'ProposalController@postEditProposal',
    'as' => 'proposal/edit',
    'middleware' => ['auth', 'roles'],
    'roles' => ['Admin', 'Member']
]);

/* Edit Comment */
Route::post('comment/edit', [
    'uses' => 'CommentController@postEditComment',
    'as' => 'comment/edit',
    'middleware' => ['auth', 'roles'],
    'roles' => ['Admin', 'Member']
]);
