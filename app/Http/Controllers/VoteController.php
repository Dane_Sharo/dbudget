<?php

namespace App\Http\Controllers;

use App\Budget;
use App\Vote;
use App\Proposal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class VoteController extends Controller
{
    public function postCreateVote(Request $request)
    {
        // Validate fields.
        $this->validate($request, [
            'proposal_id' => 'required|integer',
            'user_vote' => 'required',
        ]);

        //Check if allocation method is valid.
        $user_vote = $request['user_vote'];
        if (!($user_vote == 'agree' || $user_vote == 'disagree' || $user_vote == 'abstain'))
        {
            // Error on invalid post
            return redirect()->back()->withInput()->withErrors(
                ['invalid_vote' => 'The vote is invalid.']
            );
        }

        // Check if current time exceeds voting deadline...
        $budget = Budget::find($request['budget_id']);
        $current_time = Carbon::now();
        $vote_deadline = Carbon::parse($budget->vote_deadline);
        if ($vote_deadline->lt($current_time))
        {
            // Error on invalid post
            return redirect()->back()->withInput()->withErrors(
                ['vote_deadline' => 'The voting deadline has passed. You cannot make or change your votes.']
            );
        }

        $member_id = Auth::user()->members()->where('budget_id', '=', $request['budget_id'])->first()->id;

        //If vote exists.
        $proposal = Proposal::find($request['proposal_id']);
        $vote = $proposal->votes()->where('member_id', '=', $member_id)->first();

        // If the vote exists.
        if($vote != null)
        {
            if($request['user_vote'] == 'abstain')
            {
                $vote = Vote::find($vote->id);
                $vote->delete();

                flash('You have successfully removed your vote', 'success');
                return redirect()->back();
            }

            $vote = Vote::find($vote->id);
            $vote->user_vote = $request['user_vote'];
            $vote->save();

            flash('You have successfully changed your vote to - ' . $request['user_vote'], 'success');
            return redirect()->back();
        }

        // Make and Save new priority
        $vote = new Vote();
        $vote->proposal_id = $request['proposal_id'];
        $vote->member_id = $member_id;
        $vote->user_vote = $request['user_vote'];
        $vote->save();

        // Redirect to same page.
        flash('You have successfully voted - ' . $request['user_vote'], 'success');
        return redirect()->back();
    }
}
