
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Version 0.1
    </div>
    <!-- Default to the left -->
    <strong>Copyright © 2016 <a href="https://bitbucket.org/Dane_Sharo/">Danesh Durairetnam</a>.</strong> All rights reserved.
</footer>
