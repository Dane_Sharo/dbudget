<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{$userImage or asset ("/images/profile.png")}}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user() ? Auth::user()->name : 'Guest' }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        {{-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search..."/>
        <span class="input-group-btn">
        <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
    </span>
</div>
</form> --}}
<!-- /.search form -->

<!-- Sidebar Menu -->
<ul class="sidebar-menu">
    <li class="header">MAIN NAVIGATION</li>


    <!-- Optionally, you can add icons to the links -->
    <li>
        <a href="{{ url('home')}}">
            <i class="fa fa-home"></i> <span>Homepage</span>
        </a>
    </li>

    <li>
        <a href="{{ url('profile')}}">
            <i class="fa fa-user"></i> <span>Profile</span>
        </a>
    </li>

    <li>
        <a href="{{ url('help')}}">
            <i class="fa fa-question-circle"></i> <span>Help</span>
        </a>
    </li>


    <!-- Budget Tree -->
    @if(isset($budget) && !isset($my_budgets))
        <li class="header">CURRENT BUDGET</li>
        <li>
            <a href="{{ url('budget/' . $budget->budget_slug)}}">
                <i class="fa fa-calculator"></i> {{ $budget->budget_name }}
            </a>
        </li>

        <li>
            <a href="{{ url('budget/'. $budget->budget_slug . '/report') }}">
                <i class="fa fa-pie-chart"></i> Budget Report
            </a>
        </li>

        <li>
            <a href="{{ url('budget/'. $budget->budget_slug . '/members') }}">
                <i class="fa fa-users"></i> Budget Members
            </a>
        </li>

    @endif


</ul><!-- /.sidebar-menu -->
</section>
<!-- /.sidebar -->
</aside>
