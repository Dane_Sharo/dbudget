@include('_parts.modal_proposals')

<!-- Box -->
<a name="budget_priorities"></a>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-th-list"></i>
        <h3 class="box-title">Proposals</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>

    <div class="box-body">
        <div class="row">

            @foreach($proposals as $proposal)
                <div class="col-md-12">
                    <div class="clickable info-box bg-gray">
                        <a href="{{ url('budget/' . $budget->budget_slug . '/priority/' . $priority->priority_slug) . '/proposal/' . $proposal->proposal_slug}}" class="small-box-footer"></a>
                        <span class="info-box-icon"><i class="fa fa-lightbulb-o"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-number uppercase pull-right"><b>${{$proposal->proposal_cost}}</b></span>
                            <span class="info-box-number uppercase truncate"><b>{{$proposal->proposal_name}}</b></span>
                            <span class="info-box-text">{{strip_tags($proposal->proposal_description)}}</span>
                            <div class="progress">
                                <div class="progress-bar" style="width: {{ ($proposal->votes()->count() / $budget->members()->count()) * 100 }}%"></div>
                            </div>
                            <span class="progress-description">
                                {{ $proposal->votes()->count() }} of {{ $budget->members()->count() }} Members Voted
                            </span>
                        </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->
                </div><!-- /.col -->
            @endforeach

        </div><!-- /.row -->
    </div><!-- /.priority-body -->

    <div class="box-footer clearfix">
        {{ $proposals->links('vendor.pagination.default') }}
        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#addProposalModal">
            <i class="fa fa-plus"></i> Add Proposals
        </button>

    </div>

</div><!-- /.box -->
