@if(Auth::user())
    <div class="hidden">{{ $messages = Auth::user()->messages()->get()->sortByDesc('created_at') }}</div>
    <div class="hidden">{{ $messagecount = $messages->count() }}</div>
    <!-- Messages: style can be found in dropdown.less-->
    <li class="dropdown messages-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-bell-o"></i>
            <span class="label label-success">{{ $messagecount }}</span>
        </a>
        <ul class="dropdown-menu">
            <li class="header">You have {{ $messagecount }} messages</li>
            <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                    @foreach($messages as $message)

                        @if($message->message_type == 'Invite')
                            <li><!-- start message -->
                                <div class="hidden">{{ $budget_slug = $message->budget()->first()->budget_slug }}</div>
                                <a href="{{ url('budget/' . $budget_slug . '/join')}}">
                                    <h4 class="smallmargin">
                                        <i class="fa fa-envelope"></i> Invitation
                                        <small><i class="fa fa-clock-o"></i> {{ $message->created_at->diffForHumans() }}</small>
                                    </h4>
                                    <p class="smallmargin wrap">
                                        You have been invited to join {{ $message->budget()->first()->budget_name }}
                                    </p>
                                </a>
                            </li>
                            <!-- end message -->
                        @endif

                        @if($message->message_type == 'RoleChange')
                            <li><!-- start message -->
                                <a href="#">
                                    <h4>
                                        Support Team
                                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                    </h4>
                                    <p>Why not buy a new awesome theme?</p>
                                </a>
                            </li>
                            <!-- end message -->
                        @endif

                        @if($message->message_type == 'Kick')
                            <li><!-- start message -->
                                <a href="#">
                                    <h4>
                                        Support Team
                                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                                    </h4>
                                    <p>Why not buy a new awesome theme?</p>
                                </a>
                            </li>
                            <!-- end message -->
                        @endif

                    @endforeach
                </ul>
            </li>
            {{-- <li class="footer"><a href="#">See All Messages</a></li> --}}
        </ul>
    </li>
@endif
