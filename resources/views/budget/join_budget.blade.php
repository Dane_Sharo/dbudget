@extends('master')

@section('content')
    <!-- Page Title -->
    <div class="hidden">{{$page_title = 'Invitation - ' . $budget->budget_name }}</div>

    <div class="login-box">
        <div class="login-logo">
            <a><b>{{ $budget->budget_name }}</b></a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">You have been invited to join this budget.</p>

            <form action="{{ route('member/create') }}" method="post">
                <input type="hidden" name="budget_slug" value="{{ $budget->budget_slug }}">
                <input type="hidden" name="password" value="{{ $budget->budget_password }}">
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Join Budget</button>
                        <input type="hidden" name="_token" value="{{Session::token()}}">
                    </div>
                    <!-- /.col -->
                </div>
            </form>

        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->

@stop


@section('javascript')

    <script>
    //Insert javascript here
    </script>

@stop
