
<!-- Home / Dashboard Routes -->
@if(Request::is('home'))
    <div class="row">
        <div class="col-lg-4 col-md-6 col-xs-12">
            <!-- small box -->
            <div class="clickable info-box">
                <a href="{{ url('budget/create')}}" class="small-box-footer"></a>
                <span class="info-box-icon bg-green"><i class="fa fa-calculator"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><h4><b>Create New Budget</b></h4></span>
                    <p>Create a new budget.</p>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-md-6 col-xs-12">
            <!-- small box -->
            <div class="clickable info-box">
                <a href="#" class="small-box-footer" data-toggle="modal" data-target="#addMembersModal"></a>
                <span class="info-box-icon bg-blue"><i class="fa fa-user-plus"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><h4><b>Join Budget</b></h4></span>
                    <p>Join a Budget</p>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-md-6 col-xs-12">
            <!-- small box -->
            <div class="clickable info-box">
                <a href="{{ url('/help')}}" class="small-box-footer"></a>
                <span class="info-box-icon bg-maroon"><i class="fa fa-question-circle"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><h4><b>Help</b></h4></span>
                    <p>Open the help page.</p>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <!-- ./col -->
    </div><!-- ./row -->
@endif


@if (Route::current()->getName() == 'budget/{budget_slug}')
    <div class="row">
        <div class="col-lg-4 col-md-6 col-xs-12">
            <!-- small box -->
            <div class="clickable info-box">
                <a href="{{ url('home')}}" class="small-box-footer"></a>
                <span class="info-box-icon bg-orange"><i class="fa fa-home"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><h4><b>Homepage</b></h4></span>
                    <p>Return to homepage.</p>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-md-6 col-xs-12">
            <!-- small box -->
            <div class="clickable info-box">
                <a href="{{ url('budget/'. $budget->budget_slug . '/report')}}" class="small-box-footer"></a>
                <span class="info-box-icon bg-green"><i class="fa fa-pie-chart"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><h4><b>Budget Report</b></h4></span>
                    <p>Show spending plan.</p>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-md-6 col-xs-12">
            <!-- small box -->
            <div class="clickable info-box">
                <a href="{{ url('budget/'. $budget->budget_slug . '/members') }}" class="small-box-footer"></a>
                <span class="info-box-icon bg-blue"><i class="fa fa-users"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><h4><b>Budget Members</b></h4></span>
                    <p>Manage budget members.</p>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <!-- ./col -->
    </div><!-- ./row -->
@endif

@if (Route::current()->getName() == 'budget/{budget_slug}/priority/{priority_slug}')
    <div class="row hidden-xs hidden-sm">
        <div class="col-lg-4 col-md-6 col-xs-12">
            <!-- small box -->
            <div class="clickable info-box">
                <a href="{{ url('home')}}" class="small-box-footer"></a>
                <span class="info-box-icon bg-orange"><i class="fa fa-home"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><h4><b>Homepage</b></h4></span>
                    <p>Return to homepage.</p>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-md-6 col-xs-12">
            <!-- small box -->
            <div class="clickable info-box">
                <a href="{{ url('budget/' . $budget->budget_slug)}}" class="small-box-footer"></a>
                <span class="info-box-icon bg-green"><i class="fa  fa-calculator"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><h4><b>Budget</b></h4></span>
                    <p>Return to budget page.</p>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-md-6 col-xs-12">
            <!-- small box -->
            <div class="clickable info-box">
                <a href="#" class="small-box-footer" data-toggle="modal" data-target="#addProposalModal"></a>
                <span class="info-box-icon bg-purple"><i class="fa fa-lightbulb-o"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><h4><b>Create Proposal</b></h4></span>
                    <p>Create a new proposal</p>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <!-- ./col -->
    </div><!-- ./row -->
@endif

@if (Route::current()->getName() == 'budget/{budget_slug}/priority/{priority_slug}/proposal/{proposal_slug}')
    <div class="row hidden-xs hidden-sm">
        <div class="col-lg-4 col-md-6 col-xs-12">
            <!-- small box -->
            <div class="clickable info-box">
                <a href="{{ url('home')}}" class="small-box-footer"></a>
                <span class="info-box-icon bg-orange"><i class="fa fa-home"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><h4><b>Homepage</b></h4></span>
                    <p>Return to homepage.</p>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-md-6 col-xs-12">
            <!-- small box -->
            <div class="clickable info-box">
                <a href="{{ url('budget/' . $budget->budget_slug)}}" class="small-box-footer"></a>
                <span class="info-box-icon bg-green"><i class="fa  fa-calculator"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><h4><b>Budget</b></h4></span>
                    <p>Return to budget page.</p>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-md-6 col-xs-12">
            <!-- small box -->
            <div class="clickable info-box">
                <a href="{{ url('budget/' . $budget->budget_slug . '/priority/' . $priority->priority_slug )}}" class="small-box-footer"></a>
                <span class="info-box-icon bg-blue"><i class="fa fa-tag"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><h4><b>Priority</b></h4></span>
                    <p>Return to priority page.</p>
                </div>
                <!-- /.info-box-content -->
            </div>
        </div>
        <!-- ./col -->
    </div><!-- ./row -->
@endif
