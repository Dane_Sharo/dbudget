@extends('master')

@section('content')
    <!-- Page Title -->
    <div class="hidden">{{$page_title = 'Create Budget' }}</div>

    <div class="row">
        <div class="col-md-8">

            <!-- Box -->
            <a name="create_budget"></a>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-calculator"></i>
                    <h3 class="box-title">Create Budget</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <form action="{{ route('budget/create') }}" method="post">
                        <!-- Budget Name -->
                        <div class="form-group">
                            <label>Budget Name</label>
                            <input type="text" class="form-control" placeholder="Your budget name" name="budget_name" id="budget_name" value="{{Request::old('budget_name')}}">
                        </div>

                        <!-- Budget Description - textarea -->
                        <div class="form-group">
                            <label>Budget Description</label>
                            <textarea class="form-control" rows="3" placeholder="Description of your budget" name="budget_description" id="budget_description">{{Request::old('budget_description')}}</textarea>
                        </div>

                        <!-- Budget Funds - numeric -->
                        <div class="form-group">
                            <label>Budget Funds</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="number" class="form-control" name="budget_funds" id="budget_funds" value="{{Request::old('budget_funds')}}">
                            </div>
                        </div>

                        <!-- Budget Password -->
                        <div class="form-group">
                            <label>Budget Password</label>
                            <input type="text" class="form-control" placeholder="Your budget password" name="budget_password" id="budget_password" value="{{Request::old('budget_password')}}">
                        </div>

                        <div class="row">
                            <!-- Minimum Votes -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Minimum Votes</label>
                                    <div class="input-group">
                                        <input type="number" class="form-control" placeholder="Min votes for proposal to pass" name="min_vote" id="min_vote" value="{{Request::old('min_vote')}}">
                                        <span class="input-group-addon">#</span>
                                    </div>
                                </div>
                            </div>
                            <!-- Budget Agree % -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Minimum Agree Percentage</label>
                                    <div class="input-group">
                                        <input type="number" class="form-control" placeholder="Min % of agree votes for proposal to pass" name="min_agree_percent" id="min_agree_percent" value="{{Request::old('min_agree_percent')}}">
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </div>
                            </div>

                            <!-- Proposal Deadline -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Proposal Deadline</label>
                                    <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker1" name="proposal_deadline" value="{{Request::old('proposal_deadline')}}">
                                        <span class="input-group-addon" data-target="#datetimepicker1" data-toggle="datetimepicker">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <!-- Vote Deadline -->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Voting Deadline</label>
                                    <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker2" name="vote_deadline" value="{{Request::old('vote_deadline')}}">
                                        <span class="input-group-addon" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p> Click on calendar icon to set date/time. Format: (YYYY/MM/DD hh:mm A)</p>

                        <div class="box-footer pull-right">
                            <button type="submit" class="btn btn-primary">Create Budget</button>
                            <input type="hidden" name="_token" value="{{Session::token()}}">
                        </div>

                    </form>
                </div><!-- /.box-body -->


            </div><!-- /.box -->
        </div>

    </div><!-- /.row -->






@stop

@section('javascript')
    <!--Javascript here -->

@stop
