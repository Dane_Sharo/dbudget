@extends('master')

@section('content')

    <!-- Page Title -->
    <div class="hidden">{{$page_title = 'Budget Members - ' . $budget->budget_name}}</div>

    <div class="row">
        <div class="col-md-4">

            <!-- Box -->
            <a name="budget"></a>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">Member's Summary</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">

                    <dl class="row">
                        <dt class="col-xs-6"><i class="fa fa-fw fa-users"></i> Number of Members</dt>
                        <dd class="col-xs-6 pull-right">
                            <p class="pull-right">{{ $members->count() }}</p>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-xs-6"><i class="fa fa-fw fa-users"></i> Number of Admins</dt>
                        <dd class="col-xs-6 pull-right">
                            <p class="pull-right">{{ $members->where('role_id', '=', '1')->count() }}</p>
                        </dd>
                    </dl>

                </div>

                <div class="box-footer clearfix">

                </div>
            </div><!-- /.box -->

        </div><!-- /.col -->

        <div class="col-md-8">
            @if($isadmin)
                @include('_parts.box_invite')
                @include('_parts.modal_invite')
            @endif
        </div>

    </div>

    <div class="row">

        <div class="col-md-12">

            <!-- Box -->
            <a name="members"></a>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-users"></i>
                    <h3 class="box-title">Members</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <table id="members_table" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td class="col-md-4 col-xs-4">Member Name</td>
                                <td class="col-md-4 hidden-xs">Email</td>
                                <td class="col-md-2 col-xs-4">Role</td>
                                @if($isadmin === true)
                                    <td class="col-md-2 col-xs-4">Action</td>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($members as $member)
                                <tr>
                                    <td>{{ $member->user()->first()->name }}</td>
                                    <td class="hidden-xs">{{ $member->user()->first()->email }}</td>
                                    <td>{{ $member->role()->first()->role_name }}</td>
                                    @if($isadmin === true)
                                        <td>
                                            <form action="{{ route('member/edit') }}" method="post">
                                                <input type="hidden" name="member_id" value="{{ $member->id }}">
                                                <input type="hidden" name="budget_id" value="{{ $budget->id }}">
                                                <div class="btn-group btn-group-justified">
                                                    @if($member->role()->first()->role_name != 'Admin')
                                                        <div class="btn-group">
                                                            <button type="submit" class="btn btn-success" name="member_action" value="Admin"> Make Admin</button>
                                                        </div>
                                                    @endif

                                                    @if($member->role()->first()->role_name != 'Member')
                                                        <div class="btn-group">
                                                            <button type="submit" class="btn btn-warning" name="member_action" value="Member"> Make Member</button>
                                                        </div>
                                                    @endif
                                                </div>
                                                <input type="hidden" name="_token" value="{{Session::token()}}">
                                            </form>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div><!-- /.box-body -->

                <div class="box-footer clearfix">

                </div>

            </div><!-- /.box -->


        </div> <!-- /.col -->
    </div><!-- /.row -->


@stop

@section('javascript')
    <script>
    $(function () {
        $("#default_datatable").DataTable();
        $('#members_table').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
    </script>
@stop
