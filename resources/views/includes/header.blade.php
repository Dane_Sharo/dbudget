<header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/home')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>D</b>B</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>D</b>Budget</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                @include('includes.messages')

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{$userImage or asset ("/images/profile.png")}}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{ Auth::user() ? Auth::user()->name : 'Guest' }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{$userImage or asset ("/images/profile.png")}}" class="img-circle" alt="User Image">
                                <p>
                                    {{ Auth::user() ? Auth::user()->name : 'Guest' }}
                                    <small>Online</small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer bg-navy">
                                <div class="pull-left">
                                    <a href="{{route('profile')}}" class="btn btn-primary btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{route('user/logout')}}" class="btn btn-danger btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
                    {{-- <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li> --}}
                </ul>
            </div>
        </nav>
    </header>
