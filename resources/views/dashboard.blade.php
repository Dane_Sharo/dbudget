@extends('master')

@section('content')

    <div class="hidden">{{$page_title = 'Homepage' }}</div>
    <!-- Input important parts here -->

    <div class="row">

        @include('_parts.modal_members')

        <div class="col-md-6">
            <!-- Box -->
            <a name="sign_in"></a>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-bar-chart"></i>
                    <h3 class="box-title">My Budgets</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <table class="table table-button"><tbody>
                        <tr>
                            <th class="col-md-1 col-sm-2">
                                <h5><b>Budget Name</b></h5>
                            </th>
                            <th class="col-md-1 col-sm-1">
                                <h5><b>Budget Funds</b></h5>
                            </th>
                        </tr>

                        @foreach ($my_budgets->all() as $budget)
                            <tr>
                                <div class="hidden">{{$url = "/budget/" . $budget->budget_slug}}</div>
                                <td><a href="{{ url($url)}}" class="rowlink">
                                    <span><i class="fa fa-calculator"></i></span>
                                    <span>{{ $budget->budget_name }}</span>
                                </a></td>
                                <td class="content">
                                    <span><i class="fa fa-dollar"></i></span>
                                    <span>{{$budget->budget_funds}}</span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody></table>
                </div>
                <!-- /.login-box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->


        <div class="col-md-6">
            <!-- Box -->
            <a name="sign_in"></a>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-bar-chart"></i>
                    <h3 class="box-title">Participating Budgets</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">

                    <table class="table table-button"><tbody>
                        <tr>
                            <th class="col-md-1 col-sm-2">
                                <h5><b>Budget Name</b></h5>
                            </th>
                            <th class="col-md-1 col-sm-1">
                                <h5><b>Budget Funds</b></h5>
                            </th>
                        </tr>
                        @foreach ($joined_budgets->all() as $budget)
                            @if($budget->user_id != Auth::user()->id)
                                <tr>
                                    <div class="hidden">{{$url = "/budget/" . $budget->budget_slug}}</div>
                                    <td><a href="{{ url($url)}}" class="rowlink">
                                        <span><i class="fa fa-calculator"></i></span>
                                        <span>{{ $budget->budget_name }}</span>
                                    </a></td>
                                    <td class="content">
                                        <span><i class="fa fa-dollar"></i></span>
                                        <span>{{$budget->budget_funds}}</span>
                                    </td>
                                </tr>
                            @endif
                        @endforeach

                    </tbody></table>
                </div>
                <!-- /.login-box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->


    </div><!-- /.row -->




@stop

@section('javascript')

    <!--Javascript here -->

@stop
