<!-- Box -->
<a name="budget"></a>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-user-plus"></i>
        <h3 class="box-title">Invite New Members</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>

    <form action="{{ route('member/invite') }}" method="post">
        <div class="box-body">

            <p><b>Select Members:</b></p>


            <input type="hidden" name="budget_id" value="{{ $budget->id }}">

            <div class="form-group">
                <select class="form-control select2" name="users[]" multiple="multiple" data-placeholder="Select Members" style="width: 100%;">
                    @foreach($users as $useri)
                        @if ($members->where('user_id', '=', $useri->id)->count() < 1)
                            <option value="{{ $useri->id }}">{{ $useri->name }}({{ $useri->email }})</option>
                        @endif
                    @endforeach
                </select>
            </div>


        </div>

        <div class="box-footer clearfix">
            <button type="button" class="btn btn-primary truncate" data-toggle="modal" data-target="#showMembersModal">
                 Invite Non-Users
            </button>
            <button type="submit" class="btn btn-success truncate pull-right" name="invite_member" value="invite">
                 Invite Users
            </button>
            <input type="hidden" name="_token" value="{{Session::token()}}">

        </div>
    </form>
</div><!-- /.box -->
