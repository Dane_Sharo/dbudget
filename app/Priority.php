<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Priority extends Model
{
    public function budget()
    {
        return $this->belongsTo('App\Budget', 'budget_id');
    }

    public function proposals()
    {
        return $this->hasMany('App\Proposal');
    }

}
