@extends('master')

@section('content')
    <!-- Page Title -->
    <div class="hidden">{{$page_title = 'Budget - ' . $budget->budget_name}}</div>

    <div class="row">
        <div class="col-md-4">

            <!-- Box -->
            <a name="budget"></a>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-calculator"></i>
                    <h3 class="box-title">Budget Information</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">

                    <strong><i class="fa fa-fw fa-file-text-o"></i> Description</strong>
                    <p class="text-muted">
                        {!! $budget->budget_description !!}
                    </p>

                    <hr>
                    <dl class="row">
                        <dt class="col-xs-6"><i class="fa fa-fw fa-dollar"></i> Funds</dt>
                        <dd class="col-xs-6 pull-right">{{ $budget->budget_funds }}</dd>

                        <dt class="col-xs-6"><i class="fa fa-fw fa-inbox"></i> Min Votes</dt>
                        <dd class="col-xs-6 pull-right">{{ $budget->min_vote }}</dd>

                        <dt class="col-xs-6"><i class="fa fa-fw fa-check"></i> Min Agree %</dt>
                        <dd class="col-xs-6 pull-right">{{ $budget->min_agree_percent }}</dd>
                    </dl>

                    <hr>
                    <dl class="row">
                        <dt class="col-xs-6 truncate"><i class="fa fa-fw fa-calendar"></i> Proposal Deadline</dt>
                        <dd class="col-xs-6 truncate">{{ Carbon\Carbon::parse($budget->proposal_deadline)->format('d F Y h:i A') }}</dd>

                        <dt class="col-xs-6 truncate"><i class="fa fa-fw fa-calendar"></i> Voting Deadline</dt>
                        <dd class="col-xs-6 truncate">{{ Carbon\Carbon::parse($budget->vote_deadline)->format('d F Y h:i A') }}</dd>
                    </dl>

                </div>

                <div class="box-footer clearfix">
                    @if($isadmin)
                        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#editBudgetModal">
                            <i class="fa fa-fw fa-edit"></i> Edit Budget
                        </button>
                    @endif
                </div>
            </div><!-- /.box -->

            @include('_parts.modals_ed_budget')

        </div><!-- /.col -->


        <div class="col-md-8">

            @include('_parts.box_priorities')

        </div><!-- /.col -->


    </div><!-- /.row -->



@stop

@section('javascript')
    <!--Javascript here -->

@stop
