@extends('master')

@section('content')
    <!-- Page Title -->
    <div class="hidden">{{$page_title = 'Proposal - ' . $proposal->proposal_name}}</div>

    <div class="row">

        <div class="col-md-8">
            <!-- Box -->
            <a name="proposal"></a>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-lightbulb-o"></i>
                    <h3 class="box-title">Proposal Information</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">

                    <strong><i class="fa fa-fw fa-file-text-o"></i> Description</strong>
                    <p class="text-muted">
                        {!! $proposal->proposal_description !!}
                    </p>

                    <hr>
                    <dl class="row">
                        <dt class="col-xs-6"><i class="fa fa-fw fa-dollar"></i> Proposal Cost</dt>
                        <dd class="col-xs-6">{{ $proposal->proposal_cost }}</dd>

                        <dt class="col-xs-6"><i class="fa fa-fw fa-user"></i> Proposed By</dt>
                        <dd class="col-xs-6">{{ $proposal->member()->first()->user()->first()->name }}</dd>
                    </dl>

                </div>

                <div class="box-footer clearfix">

                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addCommentModal">
                        <i class="fa fa-fw fa-comment"></i> Comment
                    </button>

                    @if($isadmin || $iscreator)
                        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#editProposalModal">
                            <i class="fa fa-fw fa-plus"></i> Edit Proposal
                        </button>
                    @endif
                </div>

            </div><!-- /.box -->

            @include('_parts.modals_ed_proposal')
            @include('_parts.modal_veto')

        </div><!-- /.col -->

        <div class="col-md-4">

            <!-- Box -->
            <a name="votes"></a>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-check-square"></i>
                    <h3 class="box-title">Votes</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <dl class="row">
                        <dt class="col-xs-6"><i class="fa fa-fw fa-check-square"></i> Number of Votes</dt>
                        <dd class="col-xs-6">
                            <p class="pull-right">{{ $votes->count() }}</p>
                        </dd>

                        <dt class="col-xs-6"><i class="fa fa-fw fa-check"></i> Number Agree</dt>
                        <dd class="col-xs-6">
                            <p class="pull-right">{{ $votes->where('user_vote', '=', 'agree')->count() }}</p>
                        </dd>

                        <dt class="col-xs-6"><i class="fa fa-fw fa-close"></i> Number Disagree</dt>
                        <dd class="col-xs-6">
                            <p class="pull-right">{{ $votes->where('user_vote', '=', 'disagree')->count() }}</p>
                        </dd>
                    </dl>

                    <hr>
                    <strong><i class="fa fa-fw fa-legal"></i> Number of Vetos</strong>
                    <p class="text-muted pull-right">
                        <b>{{ $vetos->count() }}</b>
                    </p>

                </div>

                <div class="box-footer clearfix">

                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addVoteModal">
                        <i class="fa fa-fw fa-check-square"></i> Vote
                    </button>

                    @if($isadmin)
                        <button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#addVetoModal">
                            <i class="fa fa-fw fa-legal"></i> Veto Proposal
                        </button>
                    @endif
                </div>

            </div><!-- /.box -->
        </div><!-- /.col -->

        <div class="col-md-8">
            @include('_parts.box_comments')
            @include('_parts.modal_votes')
        </div>

        <div class="col-md-4">
            @include('_parts.box_vetos')
        </div>

    </div><!-- /.row -->



@stop

@section('javascript')
    <!--Javascript here -->

@stop
