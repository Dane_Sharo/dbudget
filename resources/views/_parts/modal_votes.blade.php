
<!-- Modal -->
<div class="modal fade" id="addVoteModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Vote</h4>
            </div>

            <!-- Modal Body -->
            <form action="{{ route('vote/create') }}" method="post">
                <div class="modal-body">

                    <input type="hidden" name="budget_id" value="{{ $budget->id }}">
                    <input type="hidden" name="priority_id" value="{{ $priority->id }}">
                    <input type="hidden" name="proposal_id" value="{{ $proposal->id }}">

                    {{-- Display message depending on if the user has already voted or not --}}
                    <div class="hidden">{{$thismember_id =  Auth::user()->members()->where('budget_id', '=', $budget->id)->first()->id}}</div>
                    @if($votes->where('member_id', '=', $thismember_id)->count() > 0)
                        <p>You have already voted "{{$votes->where('member_id', '=', $thismember_id)->first()->user_vote}}" on this proposal.</p>
                        <p>You can still change your vote. Choosing abstain will remove your current vote.</p>
                    @else
                        <p>You may choose to vote agree or disagree on this proposal.</p>
                    @endif

                    <hr>

                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <button type="submit" class="btn btn-success" name="user_vote" value="agree">Agree</button>
                        </div>
                        @if($votes->where('member_id', '=', $thismember_id)->count() > 0)
                        <div class="btn-group">
                            <button type="submit" class="btn btn-warning" name="user_vote" value="abstain">Abstain</button>
                        </div>
                        @endif
                        <div class="btn-group">
                            <button type="submit" class="btn btn-danger" name="user_vote" value="disagree">Disagree</button>
                        </div>
                    </div>

                </div>
                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <input type="hidden" name="_token" value="{{Session::token()}}">
                </div>
            </form>
        </div>
    </div>
</div>
