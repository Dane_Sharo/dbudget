
<!-- Box -->
<a name="budget_priorities"></a>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-lrgal"></i>
        <h3 class="box-title"> Vetos</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>

    <div class="box-body">

        <ul class="timeline timeline-inverse">

            @foreach($vetos as $veto)
                <!-- timeline item -->
                <li>
                    <i class="fa fa-legal bg-red"></i>
                    <div class="timeline-item">
                        {{-- <span class="time"><i class="fa fa-clock-o"></i> {{$veto->created_at->diffForHumans() /*format('j F Y - g:i a')*/}}</span> --}}
                        <h3 class="timeline-header"><a href="#">{{$veto->member()->first()->user()->first()->name}}</a></h3>
                        <div class="timeline-body">
                            {!! $veto->veto_reason !!}
                        </div>
                    </div>
                </li>
                <!-- END timeline item -->
            @endforeach
            <li>
                <i class="fa fa-clock-o bg-gray"></i>
            </li>
        </ul>


    </div><!-- /.priority-body -->

    <div class="box-footer clearfix">

    </div>

</div><!-- /.box -->
