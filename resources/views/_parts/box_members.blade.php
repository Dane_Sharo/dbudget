
<!-- Box -->
<a name="members"></a>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-users"></i>
        <h3 class="box-title">Members</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>

    <div class="box-body">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <td class="col-md-2 col-sm-2">Name</td>
                    <td class="col-md-2 col-sm-2">Email</td>
                    <td class="col-md-1 col-sm-1">Role</td>
                    <td class="col-md-1 col-sm-1">Actions</td>
                </tr>
            </thead>
            <tbody>
                @foreach($members as $member)
                    <tr>
                        <td>{{ $member->user()->first()->name }}</td>
                        <td>{{ $member->user()->first()->email }}</td>
                        <td>{{ $member->role()->first()->role_name }}</td>

                        <!-- we will also add show, edit, and delete buttons -->
                        <td>

                            <!-- delete the member (uses the destroy method DESTROY /members/{id} -->
                            <!-- we will add this later since its a little more complicated than the other two buttons -->

                            <!-- edit this member (uses the edit method found at GET /members/{id}/edit -->
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-id="{{ $member->id }}" data-email="{{$member->user()->first()->email}}"
                            data-target="#editMembersModal" id="editMemberBtn">
                                <i class="fa fa-pencil"></i>
                            </button>

                            <!-- show the member (uses the show method found at GET /members/{id} -->
                            <a class="btn btn-small btn-danger" href="{{ URL::to('members/' . $member->id . '/remove') }}"><i class="fa fa-trash"></i></a>



                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <div class="box-footer">
        </div>

        <div class="pull-right">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addMembersModal">Add Members</button>
        </div>

    </div>
    <!-- /.login-box-body -->

</div><!-- /.box -->



<script>

</script>
