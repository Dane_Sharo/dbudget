
<!-- Modal -->
<div class="modal fade" id="showMembersModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Invite Members</h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">

                <p><b>Copy the budget key and password listed below and send them to people you want to invite:</b></p>
                <p>The invited members can join this budget once they sign up and click "join budget" at their homepage.</p>

                <hr>
                <dl class="row">

                    <dt class="col-xs-6"><i class="fa fa-fw fa-key"></i> Budget Key</dt>
                    <dd class="col-xs-6 pull-right">{{ $budget->budget_slug }}</dd>
                </dl>
                <dl class="row">
                    <dt class="col-xs-6"><i class="fa fa-fw fa-lock"></i> Password</dt>
                    <dd class="col-xs-6 pull-right">{{ $budget->budget_password }}</dd>
                </dl>

            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
                <input type="hidden" name="_token" value="{{Session::token()}}">
            </div>
        </div>
    </div>
</div>
