<?php

namespace App\Http\Controllers;

use App\Budget;
use App\Priority;
use App\Classes\RoleCheck;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PriorityController extends Controller
{
    public function validatePriority(Request $request)
    {
        // Validate fields.
        $this->validate($request, [
            'budget_id' => 'required|integer',
            'priority_name' => 'required|max:30',
            'priority_description' => 'required|max:25000',
            'max_percentage' => 'required|integer|min:1|max:100'
        ]);

        // Validation success...
        return "valid";
    }

    public function postCreatePriority(Request $request)
    {
        // Call validate function...
        if ($this->validatePriority($request) != "valid")
        {
            return redirect()->back();
        }

        // Generate slug with timestamp.
        $priority_slug = time() . "-" . str_slug($request['priority_name'], "-");

        // If slug already exists, generate new slug with new time.
        // Incase two priorities with same name get created at the same second...
        while (Priority::where('priority_slug', '=', $priority_slug)->count() > 0){
            $priority_slug = time() . "-" . str_slug($request['priority_name'], "-");
        }

        // Make and Save new priority
        $priority = new Priority();
        $priority->budget_id = $request['budget_id'];
        $priority->priority_slug = $priority_slug;
        $priority->priority_name = $request['priority_name'];
        $priority->priority_description = clean( $request['priority_description'] );
        $priority->max_percentage = $request['max_percentage'];
        $priority->save();

        // Flash success message and redirect to to created priority
        flash('You have successfully created a new priority.', 'success');
        return redirect()->back();
    }

    public function postEditPriority(Request $request)
    {
        // Call validate function...
        if ($this->validatePriority($request) != "valid")
        {
            return redirect()->back();
        }

        // Get priority id...
        $priority_id = $request['priority_id'];
        // Make and Save new priority
        $priority = Priority::find($priority_id);
        $priority->priority_name = $request['priority_name'];
        $priority->priority_description = clean( $request['priority_description'] );
        $priority->max_percentage = $request['max_percentage'];
        $priority->save();

        // Flash success message and redirect to to created priority
        flash('You have successfully edited the priority.', 'success');
        return redirect()->back();
    }

    public function getPriority($budget_slug,$priority_slug)
    {
        $budget = Budget::where('budget_slug', '=', $budget_slug)->first();
        $isadmin = RoleCheck::isAdmin($budget, Auth::user());
        $priority = $budget->priorities()->where('priority_slug', '=', $priority_slug)->first();

        if ($priority === null)
        {
            return abort(404);
        }

        $proposals = $priority->proposals()->orderBy('created_at', 'desc')->paginate(10);
        return view('budget/priority')->with([
            'budget' => $budget,
            'isadmin' => $isadmin,
            'priority' => $priority,
            'proposals' => $proposals
        ]);
    }
}
