
@if (Route::current()->getName() == 'budget/{budget_slug}')
        <ol class="breadcrumb">
            <li class="active">
                <a><i class="fa fa-calculator"></i> {{ $budget->budget_name }}</a>
            </li>
        </ol>
@endif


@if (Route::current()->getName() == 'budget/{budget_slug}/report')
        <ol class="breadcrumb">
            <li><a href="{{ url('budget/' . $budget->budget_slug)}}">
                <i class="fa fa-calculator"></i> {{ $budget->budget_name }}</a>
            </li>
            <li class="active">
                <a><i class="fa fa-pie-chart"></i>  Budget Report</a>
            </li>
        </ol>
@endif


@if (Route::current()->getName() == 'budget/{budget_slug}/priority/{priority_slug}')
        <ol class="breadcrumb">
            <li><a href="{{ url('budget/' . $budget->budget_slug)}}">
                <i class="fa fa-calculator"></i> {{ $budget->budget_name }}</a>
            </li>
            <li class="active">
                <a><i class="fa fa-tag"></i>  {{ $priority->priority_name }}</a>
            </li>
        </ol>
@endif


@if (Route::current()->getName() == 'budget/{budget_slug}/priority/{priority_slug}/proposal/{proposal_slug}')
        <ol class="breadcrumb">
            <li><a href="{{ url('budget/' . $budget->budget_slug)}}">
                <i class="fa fa-calculator"></i> {{ $budget->budget_name }}</a>
            </li>
            <li><a href="{{ url('budget/' . $budget->budget_slug . '/priority/' . $priority->priority_slug )}}">
                <i class="fa fa-tag"></i> {{ $priority->priority_name }}</a>
            </li>
            <li class="active">
                <a><i class="fa fa-lightbulb-o"></i> {{ $proposal->proposal_name }}</a>
            </li>
        </ol>
@endif
