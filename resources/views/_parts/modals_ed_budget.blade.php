
<!-- Modal -->
<div class="modal fade" id="editBudgetModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Edit Budget</h4>
            </div>

            <!-- Modal Body -->
            <form action="{{ route('budget/edit') }}" method="post">
                <div class="modal-body">

                    <input type="hidden" name="budget_id" value="{{ $budget->id }}">

                    <!-- Budget Name -->
                    <div class="form-group">
                        <label>Budget Name</label>
                        <input type="text" class="form-control" placeholder="Your budget name" name="budget_name" id="budget_name" value="{{ $budget->budget_name }}">
                    </div>

                    <!-- Budget Description - textarea -->
                    <div class="form-group">
                        <label>Budget Description</label>
                        <textarea class="form-control" rows="3" placeholder="Description of your budget" name="budget_description" id="budget_description">{{ $budget->budget_description }}</textarea>
                    </div>

                    <!-- Budget Funds - numeric -->
                    <div class="form-group">
                        <label>Budget Funds</label>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="number" class="form-control" name="budget_funds" id="budget_funds" value="{{ $budget->budget_funds }}">
                        </div>
                    </div>

                    <!-- Budget Password -->
                    <div class="form-group">
                        <label>Budget Password</label>
                        <input type="text" class="form-control" placeholder="Your budget password" name="budget_password" id="budget_password" value="{{ $budget->budget_password }}">
                    </div>

                    <div class="row">
                        <!-- Minimum Votes -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Minimum Votes</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" placeholder="Min votes for proposal to pass" name="min_vote" id="min_vote" value="{{ $budget->min_vote }}">
                                    <span class="input-group-addon">#</span>
                                </div>
                            </div>
                        </div>
                        <!-- Budget Agree % -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Minimum Agree Percentage</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" placeholder="Min % of agree votes for proposal to pass" name="min_agree_percent" id="min_agree_percent" value="{{ $budget->min_agree_percent }}">
                                    <span class="input-group-addon">%</span>
                                </div>
                            </div>
                        </div>

                        <!-- Proposal Deadline -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Proposal Deadline</label>
                                <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker1" name="proposal_deadline" value="{{ $budget->proposal_deadline }}">
                                    <span class="input-group-addon" data-target="#datetimepicker1" data-toggle="datetimepicker">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <!-- Vote Deadline -->
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Voting Deadline</label>
                                <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-target="#datetimepicker2" name="vote_deadline" value="{{ $budget->vote_deadline }}">
                                    <span class="input-group-addon" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p> Click on calendar icon to set date/time. Format: (YYYY/MM/DD hh:mm A)</p>

                </div>
                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <input type="hidden" name="_token" value="{{Session::token()}}">
                </div>
            </form>
        </div>
    </div>
</div>
