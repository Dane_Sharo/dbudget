<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function members()
    {
        return $this->hasMany('App\Member');
    }

    public function priorities()
    {
        return $this->hasMany('App\Priority');
    }

    public function messages()
    {
        return $this->hasMany('App\Message');
    }

}
