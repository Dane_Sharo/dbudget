<?php

namespace App\Http\Controllers;

use App\Budget;
use App\Priority;
use App\Proposal;
use App\Classes\RoleCheck;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ProposalController extends Controller
{
    public function validateProposal(Request $request)
    {
        // Validate fields.
        $this->validate($request, [
            'priority_id' => 'required|integer',
            'proposal_name' => 'required|max:50',
            'proposal_description' => 'required|max:25000',
            'proposal_cost' => 'required|numeric|min:1|max:99999999'
        ]);

        // Check if proposal cost exceed priority funds.
        $budget = Budget::find($request['budget_id']);
        $budgetfunds = $budget->budget_funds;
        $percentage = Priority::find($request['priority_id'])->max_percentage;
        $priority_fund = $budgetfunds * ($percentage / 100);
        if ($request['proposal_cost'] > $priority_fund)
        {
            // Error on invalid post
            return redirect()->back()->withInput()->withErrors(
                ['cost_exceed' => 'The proposal cost cannot exceed the priority funds of $' . $priority_fund]
            );
        }

        // Check if current time exceeds proposal deadline...
        $current_time = Carbon::now();
        $proposal_deadline = Carbon::parse($budget->proposal_deadline);
        if ($proposal_deadline->lt($current_time))
        {
            // Error on invalid post
            return redirect()->back()->withInput()->withErrors(
                ['proposal_deadline' => 'The proposal deadline has passed. You cannot add or edit proposals.']
            );
        }

        // Validation success...
        return "valid";
    }

    public function postCreateProposal(Request $request)
    {
        // Call validate function...
        if ($this->validateProposal($request) != "valid")
        {
            return redirect()->back();
        }

        // Get member ID...
        $member_id = Auth::user()->members()->where('budget_id', '=', $request['budget_id'])->first()->id;

        // Generate slug with timestamp.
        $proposal_slug = time() . "-" . str_slug($request['proposal_name'], "-");

        // If slug already exists, generate new slug with new time.
        // Incase two priorities with same name get created at the same second...
        while (Proposal::where('proposal_slug', '=', $proposal_slug)->count() > 0){
            $proposal_slug = time() . "-" . str_slug($request['proposal_name'], "-");
        }

        // Make and Save new priority
        $proposal = new Proposal();
        $proposal->priority_id = $request['priority_id'];
        $proposal->member_id = $member_id;
        $proposal->proposal_slug = $proposal_slug;
        $proposal->proposal_name = $request['proposal_name'];
        $proposal->proposal_description = clean ($request['proposal_description']);
        $proposal->proposal_cost = $request['proposal_cost'];
        $proposal->save();

        // Flash success message and redirect to to created priority
        flash('You have successfully created a new proposal.', 'success');
        return redirect()->back();

    }

    public function postEditProposal(Request $request)
    {
        // Call validate function...
        if ($this->validateProposal($request) != "valid")
        {
            return redirect()->back();
        }

        // Get priority id...
        $proposal_id = $request['proposal_id'];
        // Make and Save new priority
        $proposal = Proposal::find($proposal_id);
        $proposal->priority_id = $request['priority_id'];
        $proposal->proposal_name = $request['proposal_name'];
        $proposal->proposal_description = clean ($request['proposal_description']);
        $proposal->proposal_cost = $request['proposal_cost'];
        $proposal->save();

        // Flash success message and redirect to to created priority
        flash('You have successfully edited the proposal.', 'success');
        return redirect()->route('budget/{budget_slug}/priority/{priority_slug}/proposal/{proposal_slug}', [
            'budget_slug' => Budget::find($request['budget_id'])->budget_slug,
            'priority_slug' => Priority::find($request['priority_id'])->priority_slug,
            'proposal_slug' => $proposal->proposal_slug
        ]);

    }

    public function getProposal($budget_slug,$priority_slug,$proposal_slug)
    {
        $budget = Budget::where('budget_slug', '=', $budget_slug)->first();
        $isadmin = RoleCheck::isAdmin($budget, Auth::user());
        $priorities = $budget->priorities()->get();
        $priority = $budget->priorities()->where('priority_slug', '=', $priority_slug)->first();

        if ($priority === null)
        {
            return abort(404);
        }

        $proposal = $priority->proposals()->where('proposal_slug', '=', $proposal_slug)->first();

        if ($proposal === null)
        {
            return abort(404);
        }

        if (Auth::user()->members()->where('budget_id', '=', $budget->id)->first()->id == $proposal->member_id)
        {
            $iscreator = true;
        }
        else
        {
            $iscreator = false;
        }

        $votes = $proposal->votes()->get();
        $vetos = $proposal->vetos()->get();
        $comments= $proposal->comments()->orderBy('created_at', 'desc')->paginate(10);

        return view('budget/proposal')->with([
            'budget' => $budget,
            'isadmin' => $isadmin,
            'priorities' => $priorities,
            'priority' => $priority,
            'proposal' => $proposal,
            'iscreator' => $iscreator,
            'votes' => $votes,
            'vetos' => $vetos,
            'comments' => $comments
        ]);
    }
}
