<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    public function proposal()
    {
        return $this->belongsTo('App\Proposal', 'proposal_id');
    }

    public function member()
    {
        return $this->belongsTo('App\Member', 'member_id');
    }
}
