<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    public function budget()
    {
        return $this->belongsTo('App\Budget', 'budget_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function role()
    {
        return $this->belongsTo('App\Role', 'role_id');
    }

    public function proposals()
    {
        return $this->hasMany('App\Proposal');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function votes()
    {
        return $this->hasMany('App\Vote');
    }

    public function vetos()
    {
        return $this->hasMany('App\Veto');
    }

}
