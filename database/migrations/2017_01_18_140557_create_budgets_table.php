<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetsTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('budgets', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('budget_slug')->unique();
            $table->string('budget_name');
            $table->text('budget_description');
            $table->decimal('budget_funds', 10, 2);
            $table->string('budget_password');
            $table->integer('min_vote');
            $table->integer('min_agree_percent');
            $table->timestamp('proposal_deadline');
            $table->timestamp('vote_deadline');
            $table->timestamps();
        });
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists('budgets');
    }
}
