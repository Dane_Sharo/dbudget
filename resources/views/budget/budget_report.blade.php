@extends('master')

@section('content')
    <!-- Page Title -->
    <div class="hidden">{{$page_title = 'Budget Report - ' . $budget->budget_name}}</div>

    <div class="row">

        <div class="col-md-6">
            <!-- Box -->
            <a name="budget_report"></a>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-info-circle"></i>
                    <h3 class="box-title">Budget Summary</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <dl class="row">
                        <dt class="col-xs-6"><i class="fa fa-fw fa-lightbulb-o"></i> Total Proposals</dt>
                        <dd class="col-xs-6 pull-right">
                            <p class="pull-right">{{ $proposal_count }}</p>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-xs-6"><i class="fa fa-fw fa-check"></i> Passed Proposals</dt>
                        <dd class="col-xs-6 pull-right">
                            <p class="pull-right">{{ $finalproposals->count() }}</p>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-xs-6"><i class="fa fa-fw fa-legal"></i> Vetoed Proposals</dt>
                        <dd class="col-xs-6 pull-right">
                            <p class="pull-right">{{ $veto_count }}</p>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-xs-6"><i class="fa fa-fw fa-dollar"></i> Total Funds</dt>
                        <dd class="col-xs-6 pull-right">
                            <p class="pull-right">{{ number_format($budget->budget_funds, 2, '.', '') }}</p>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-xs-6"><i class="fa fa-fw fa-line-chart"></i> Spent Funds</dt>
                        <dd class="col-xs-6 pull-right">
                            <p class="pull-right">{{ number_format($budget->budget_funds - $unused_funds, 2, '.', '') }}</p>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-xs-6"><i class="fa fa-fw fa-money"></i> Unused Funds</dt>
                        <dd class="col-xs-6 pull-right">
                            <p class="pull-right">{{ number_format($unused_funds, 2, '.', '') }}</p>
                        </dd>
                    </dl>

                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- .col -->

        <div class="col-md-6">
            @include('_parts.box_chart')
        </div><!-- .col -->

        <div class="col-md-12">

            <!-- Box -->
            <a name="budget_report"></a>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-check-square"></i>
                    <h3 class="box-title">Spending Plan</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <table id="budget_report" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td class="col-md-4 col-sm-4">Proposal Name</td>
                                <td class="col-md-4 col-sm-4 hidden-xs">Priority</td>
                                <td class="col-md-2 col-sm-2">Cost</td>
                                <td class="col-md-2 col-sm-2 hidden-xs">Votes</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($finalproposals as $fproposal)
                                <tr>
                                    <div class="hidden">{{$url = "/budget/" . $budget->budget_slug . "/priority/" . $fproposal->priority()->first()->priority_slug . "/proposal/" . $fproposal->proposal_slug }}</div>
                                    <td><a href="{{ url($url)}}" class="rowlink">
                                        {{ $fproposal->proposal_name }}
                                    </td>
                                    <td class="hidden-xs">{{ $fproposal->priority()->first()->priority_name }}</td>
                                    <td>{{ $fproposal->proposal_cost }}</td>
                                    <td  class="hidden-xs">{{ $fproposal->votes()->count() }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div><!-- /.box-body -->
            </div><!-- /.box -->



        </div> <!-- /.col -->

    </div><!-- /.row -->



@stop

@section('javascript')
    <script>
    $(function () {
        $("#default_datatable").DataTable();
        $('#budget_report').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": false,
            "autoWidth": false
        });
    });
    </script>
@stop
