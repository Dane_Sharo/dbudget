<!-- Box -->
<a name="budget_priorities"></a>
<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-pie-chart"></i>
        <h3 class="box-title">Graph</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>

    <div class="box-body">

        <div id="chart"></div>

        <script>
        var chart = c3.generate({
            data: {
                columns: [
                    @foreach($finalproposals as $proposal)
                    ['{{$proposal->proposal_name}}', {{$proposal->proposal_cost}}],
                    @endforeach
                    ['Unused Funds', {{ $unused_funds }}]
                ],
                type: 'pie',
                colors: {
                    'Unused Funds': '#333333',
                },
                color: function (color, d) {
                    return d3.rgb(color).brighter(1);
                }
            },
            pie: {
                label: {
                    format: function(value, ratio, id) {
                        return d3.format('%')(ratio);
                    }
                }
            },
            tooltip: {
                format: {
                    value: function (value, ratio, id) {
                        var format = d3.format('$');
                        return format(value);
                    }
                    //            value: d3.format(',') // apply this format to both y and y2
                }
            },
            legend: {
                show: false
            }
        });
        </script>

        <p style="text-align:center;"><b>Chart of final spending plan. Click or hover on a slice to view the details.</b></p>

    </div><!-- /.priority-body -->
</div><!-- /.box -->
