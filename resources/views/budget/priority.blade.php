@extends('master')

@section('content')
    <!-- Page Title -->
    <div class="hidden">{{$page_title = 'Priority - ' . $priority->priority_name}}</div>

    <div class="row">

        <div class="col-md-4">

            <!-- Box -->
            <a name="priority"></a>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-tag"></i>
                    <h3 class="box-title">Priority Information</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">

                    <strong><i class="fa fa-fw fa-file-text-o"></i> Description</strong>
                    <p class="text-muted">
                        {!! $priority->priority_description !!}
                    </p>

                    <hr>

                    <dl class="row">
                        <dt class="col-xs-6"><i class="fa fa-fw fa-pie-chart"></i> Max Percentage</dt>
                        <dd class="col-xs-6 pull-right">{{ $priority->max_percentage }}</dd>

                        <dt class="col-xs-6"><i class="fa fa-fw fa-dollar"></i> Max Funds Available</dt>
                        <dd class="col-xs-6">{{$budget->budget_funds * ($priority->max_percentage / 100) }}</dd>

                    </dl>

                </div>

                <div class="box-footer clearfix">
                    @if($isadmin)
                        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#editPriorityModal">
                            <i class="fa fa-fw fa-edit"></i> Edit Priority
                        </button>
                    @endif
                </div>

            </div><!-- /.box -->

            @include('_parts.modals_ed_priority')

        </div><!-- /.col -->


        <div class="col-md-8">
            @include('_parts.box_proposals')
        </div><!-- /.col -->



    </div><!-- /.row -->


@stop

@section('javascript')
    <!--Javascript here -->

@stop
