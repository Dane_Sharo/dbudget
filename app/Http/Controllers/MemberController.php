<?php

namespace App\Http\Controllers;

use App\User;
use App\Budget;
use App\Member;
use App\Role;
use App\Message;
use App\Classes\RoleCheck;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MemberController extends Controller
{
    public function postCreateMember(Request $request)
    {
        $budget_slug = $request['budget_slug'];
        $budget_password = $request['password'];
        $budget = Budget::where('budget_slug', '=', $budget_slug)->first();

        // Get user ID
        $user_id = Auth::user()->id;

        if ($budget === null)
        {
            // Error on invalid budget
            return redirect()->back()->withInput()->withErrors(
                ['invalid_budget' => 'The budget is invalid.']
            );
        }

        if ($budget->budget_password != $budget_password)
        {
            // Error on invalid password
            return redirect()->back()->withInput()->withErrors(
                ['invalid_password' => 'The budget password is invalid.']
            );
        }

        $user_inbudget = $budget->members()->where('user_id', '=', $user_id)->first();
        if ($user_inbudget != null)
        {
            // Error on invalid password
            return redirect()->back()->withInput()->withErrors(
                ['member_exist' => 'You have already joined this budget.']
            );
        }

        // Make member in Budget.
        $member = new Member();
        $member->budget_id = $budget->id;
        $member->user_id = $user_id;
        $member->role_id = Role::where('role_name', '=', 'Member')->first()->id;
        $member->save();

        // Flash success message and redirect to to created budget
        flash('You have successfully joined the budget.', 'success');
        return redirect()->route('budget/{budget_slug}', ['budget_slug' => $budget_slug]);
    }

    public function postEditMember(Request $request)
    {
        $member_action = $request['member_action'];
        if (!($member_action == 'Admin' || $member_action == 'Member'))
        {
            // Error on invalid post
            return redirect()->back()->withInput()->withErrors(
                ['invalid_action' => 'The action is invalid.']
            );
        }

        $budget = Budget::find($request['budget_id']);
        $member = Member::find($request['member_id']);
        // Prevent budget owner from removing admin role.
        if ($budget->user_id == $member->user()->first()->id)
        {
            // Return with error message.
            return redirect()->back()->withInput()->withErrors(
                ['owner_cannot_change' => 'Admin privileges cannot be removed from budget owners.']
            );
        }

        // Change and save mamber role.
        $member->role_id = Role::where('role_name', '=', $member_action)->first()->id;
        $member->save();

        // Flash success message and redirect to to budget member page
        flash('You have successfully changed the member role.', 'success');
        return redirect()->route('budget/{budget_slug}/members', ['budget_slug' => $budget->budget_slug]);
    }

    public function postInviteMember(Request $request)
    {
        $users = $request['users'];
        $budget_id = $request['budget_id'];

        if($users === null){
            // Return with error message.
            return redirect()->back()->withInput()->withErrors(
                ['no_users' => 'No users were selected. Please select users to proceed with invitation.']
            );
        }

        foreach($users as $user_id){
            // Skip if member already joined.
            if (Budget::find($budget_id)->members()->where('user_id', '=', $user_id)->count() > 0)
            {
                continue;
            }

            // Skip if member already invited.
            if (Message::where('user_id', '=', $user_id)
            ->where('budget_id', '=', $budget_id)
            ->where('message_type', '=', 'Invite')->count() > 0)
            {
                continue;
            }

            $message = new Message();
            $message->user_id = $user_id;
            $message->budget_id = $budget_id;
            $message->message_type = 'Invite';
            $message->save();
        }

        // Flash success message and redirect to to budget member page
        flash( 'You have successfully invited new member(s).', 'success');
        return redirect()->back();
    }

    public function getBudgetMembers($budget_slug)
    {
        $budget = Budget::where('budget_slug', '=', $budget_slug)->first();
        $isadmin = RoleCheck::isAdmin($budget, Auth::user());
        $members = $budget->members()->get();
        $users = User::all();

        return view('budget/members')->with([
            'budget' => $budget,
            'isadmin' => $isadmin,
            'members' => $members,
            'users' => $users
        ]);
    }

    public function getBudgetJoin($budget_slug)
    {
        $budget = Budget::where('budget_slug', '=', $budget_slug)->first();
        $user_id = Auth::user()->id;

        // Redirect if mamber is not invited.
        if (Message::where('user_id', '=', $user_id)
        ->where('budget_id', '=', $budget->id)
        ->where('message_type', '=', 'Invite')->count() < 1)
        {
            // Redirect to welcome page...
            return redirect()->route('home')->withInput()->withErrors(
                ['permission' => 'You must be invited to do that.']
            );
        }

        // Redirect to budget if member already joined.
        if (Member::where('user_id', '=', $user_id)
        ->where('budget_id', '=', $budget->id)->count() > 0)
        {
            // Redirect to welcome page...
            flash( 'You have already joined this budget.', 'success');
            return redirect()->action(
                'BudgetController@getBudget', ['budget_slug' => $budget->budget_slug]
            );
        }

        return view('budget/join_budget')->with([
            'budget' => $budget,
        ]);
    }

}
