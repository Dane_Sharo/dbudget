
<!-- Modal -->
<div class="modal fade" id="addProposalModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add Proposal</h4>
            </div>

            <!-- Modal Body -->
            <form action="{{ route('proposal/create') }}" method="post">
                <div class="modal-body">

                    <input type="hidden" name="budget_id" value="{{ $budget->id }}">
                    <input type="hidden" name="priority_id" value="{{ $priority->id }}">

                    <div class="form-group">
                        <label for="ProposalName">Proposal Name</label>
                        <input type="text" class="form-control" name="proposal_name" id="proposal_name" placeholder="Proposal Name" value="{{Request::old('proposal_name')}}">
                    </div>

                    <div class="form-group">
                        <label for="ProposalName">Proposal Description</label>
                        <textarea class="form-control" rows="3" placeholder="Description of this proposal" name="proposal_description" id="proposal_description">{{Request::old('proposal_description')}}</textarea>
                    </div>

                    <!-- Proposal Cost - numeric -->
                    <div class="form-group">
                        <label>Proposal Cost</label>
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="number" class="form-control" name="proposal_cost" id="proposal_cost" value="{{Request::old('proposal_cost')}}">
                        </div>
                    </div>

                </div>
                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <input type="hidden" name="_token" value="{{Session::token()}}">
                </div>
            </form>
        </div>
    </div>
</div>
