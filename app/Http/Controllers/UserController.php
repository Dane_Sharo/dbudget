<?php

namespace App\Http\Controllers;

use App\User;
use App\Budget;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    public function postSignUp(Request $request)
    {
        // Validate fields.
        $this->validate($request, [
            'name' => 'required|max:50',
            'email' => 'required|email|max:50|unique:users',
            'password' => 'required|min:6|max:25|confirmed',
            'password_confirmation' => 'required'
        ]);

        // Create and save new user.
        $user = new User();
        $user->email = $request['email'];
        $user->name = $request['name'];
        $user->password = bcrypt($request['password']);
        $user->save();

        // Login user
        Auth::login($user);
        // Flash success message and redirect to home.
        flash('You have successfully signed up.', 'success');
        return redirect()->route('home');

    }

    public function postSignIn(Request $request)
    {
        // Validate fields.
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $email = $request['email'];
        $password = $request['password'];

        // Authenticate User Login.
        if (Auth::attempt(['email' => $email, 'password' => $password]))
        {
            // Flash success message and redirect to home.
            flash('You have successfully signed in.', 'success');
            return redirect()->route('home');
        }

        // Error on failed login.
        return redirect()->back()->withInput()->withErrors(
            ['password' => 'Incorrect username or password.']
        );

    }

    public function postEditProfile(Request $request)
    {
        // Validate fields.
        $this->validate($request, [
            'name' => 'required|max:50',
            'email' => 'required|email|max:50',
        ]);

        // Get user id.
        $user_id = Auth::user()->id;
        // Create and save new user.
        $user = User::find($user_id);
        $user->email = $request['email'];
        $user->name = $request['name'];
        $user->save();

        // Login user
        Auth::login($user);
        // Flash success message and return to profile.
        flash('You have successfully edited yout profile.', 'success');
        return redirect()->route('profile');

    }

    public function postChangePassword(Request $request)
    {
        // Validate fields.
        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);

        // Get user email and old password.
        $email = Auth::user()->email;
        $user_id = Auth::user()->id;
        $old_password = $request['old_password'];
        // Change pasword if old password is valid.
        if (Auth::attempt(['email' => $email, 'password' => $old_password]))
        {
            // Create and save new user.
            $user = User::find($user_id);
            $user->password = bcrypt($request['password']);
            $user->save();

            // Login user
            Auth::login($user);
            // Flash success message and return to profile.
            flash('You have successfully changed your password.', 'success');
            return redirect()->route('profile');
        }

        // Error on failed login.
        return redirect()->back()->withInput()->withErrors(
            ['old_password' => 'Your old password does not match.']
        );

    }

    public function getLogout(Request $request)
    {
        Auth::logout();
        return redirect()->route('/');
    }


    public function getDashboard(Request $request)
    {
        $my_budgets = Auth::user()->budgets()->get();
        $memberships = Auth::user()->members()->get();

        $joined_budgets = collect(new Budget);
        foreach ($memberships as $membership)
        {
            $joined_budgets = $joined_budgets->merge($membership->budget()->get());
        }

        return view('dashboard')->with([
            'my_budgets' => $my_budgets,
            'joined_budgets' => $joined_budgets,
        ]);
    }

    public function getProfile(Request $request)
    {
        $my_budgets = Auth::user()->budgets()->get();
        $my_profile = Auth::user();
        return view('profile')->with([
            'my_budgets' => $my_budgets,
            'my_profile' => $my_profile
        ]);
    }
}
