@extends('master')

@section('content')

    <div class="hidden">{{$page_title = 'Profile' }}</div>
    <!-- Input important parts here -->

    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="{{$userImage or asset ("/images/profile.png")}}" alt="User profile picture">

                    <h3 class="profile-username text-center">
                        {{ Auth::user() ? Auth::user()->name : 'Guest' }}
                    </h3>

                    <div class="box-body">

                        <hr>
                        <strong><i class="fa fa-envelope margin-r-5"></i> Email</strong>
                        <p class="text-muted">
                            {{ $my_profile->email }}
                        </p>

                    </div>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="col-md-9">
            <!-- Box -->
            <a name="edit_profile"></a>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>
                    <h3 class="box-title">My Profile</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="tab-pane active" id="settings">
                        <form class="form-horizontal" action="{{ route('user/edit_profile') }}" method="post">

                            <div class="form-group">
                                <label for="inputName" class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10 has-feedback">
                                    <input type="text" class="form-control" placeholder="Full name" name="name" id="name" value="{{ $my_profile->name }}">
                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10 has-feedback">
                                    <input type="email" class="form-control" placeholder="Email" name="email" id="email" value="{{ $my_profile->email }}">
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-danger"> Submit Changes</button>
                                    <input type="hidden" name="_token" value="{{Session::token()}}">
                                </div>
                            </div>

                        </form>
                    </div>

                </div>

            </div><!-- /.box -->
        </div><!-- /.col -->

        <div class="col-md-9">
            <!-- Box -->
            <a name="change_password"></a>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-user"></i>
                    <h3 class="box-title">My Profile</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="tab-pane active" id="settings">
                        <form class="form-horizontal" action="{{ route('user/change_password') }}" method="post">

                            <div class="form-group">
                                <label for="inputPassword" class="col-sm-2 control-label">Old Password</label>
                                <div class="col-sm-10 has-feedback">
                                    <input type="password" class="form-control" placeholder="Old Password" name="old_password" id="old_password">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputPassword" class="col-sm-2 control-label">New Password</label>
                                <div class="col-sm-10 has-feedback">
                                    <input type="password" class="form-control" placeholder="Password" name="password" id="password">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputPasswordConfirmation" class="col-sm-2 control-label"></label>
                                <div class="col-sm-10 has-feedback">
                                    <input type="password" class="form-control" placeholder="Retype password" name="password_confirmation" id="password_confirmation">
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-danger"> Change Password</button>
                                    <input type="hidden" name="_token" value="{{Session::token()}}">
                                </div>
                            </div>

                        </form>
                    </div>

                </div>

            </div><!-- /.box -->
        </div><!-- /.col -->

    </div>














@stop

@section('javascript')

    <!--Javascript here -->

@stop
