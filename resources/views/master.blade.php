<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $page_title or "Page Title" }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/bootstrap/css/bootstrap.min.css") }}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.css")}}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/plugins/select2/select2.min.css")}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/dist/css/AdminLTE.min.css")}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset("/bower_components/admin-lte/dist/css/skins/_all-skins.min.css")}}">
    <!--Additional CSS-->

    <!-- Load c3.css -->
    <link rel="stylesheet" href="{{ asset("/c3-0.4.11/c3.css")}}" type="text/css">
    <!-- Load d3.js and c3.js -->
    <script src="https://d3js.org/d3.v3.min.js" charset="utf-8"></script>
    <script src="{{ asset("/c3-0.4.11/c3.min.js") }}"></script>

    <!-- DateTime Picker -->
    <link rel="stylesheet" href="{{ asset("/datetimepicker/css/bootstrap-datetimepicker.min.css")}}" />

    <link rel="stylesheet" href="{{ asset("/css/clickable.css")}}">
    <link rel="stylesheet" href="{{ asset("/css/custom.css")}}">

    {{-- TinyMce Plugin --}}
    <script src="{{ asset("/tinymce/tinymce.min.js") }}"></script>
    <script>
    tinymce.init({
        selector:'textarea',
        plugins: 'link autolink lists table textcolor image imagetools',
        browser_spellcheck: true,
        menubar: false,
        height: "200",
        toolbar: 'insertfile undo redo | bold italic forecolor | link image | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | print preview media fullpage | emoticons',
    });
    </script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
    <div class="wrapper">

        <!-- Header -->
        @include('includes.header')

        <!-- Sidebar -->
        @include('includes.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1 class="truncate">
                    {{ $page_title or "Page Title" }}
                    <small>{{ $page_description or null }}</small>
                </h1>
                @include('includes.breadcrumb')
            </section>

            <!-- Main content -->
            <section class="content">
                <!-- Your Page Content Here -->

                <!-- Error message -->
                @if(count($errors) > 0)
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-ban"></i> Error!</h4>
                            {{$error}}
                        </div>
                    @endforeach
                @endif

                <!-- Flash Message -->
                @if(session()->has('flash_notification.message'))
                    <div class="alert alert-{{ session('flash_notification.level') }} alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h4><i class="icon fa fa-check"></i> Success!</h4>
                        {{ session('flash_notification.message') }}
                    </div>
                @endif

                <!-- Modal -->
                @include('includes.modal')

                <!-- Show navigation buttons -->
                @include('includes.navigation')

                <!-- Yield content from layouts-->
                @yield('content')

            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <!-- Footer -->
        @include('includes.footer')

        @include('includes.control_sidebar')

    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.2.3 -->
    {{-- <script src="{{ asset ("/bower_components/admin-lte/plugins/jQuery/jQuery-2.2.3.min.js") }}"></script> --}}
    <script src="{{ asset("/js/jquery-3.1.1.min.js")}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
    $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.6 JS -->
    <script src="{{ asset ("/bower_components/admin-lte/bootstrap/js/bootstrap.min.js") }}" type="text/javascript"></script>

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
    Both of these plugins are recommended to enhance the
    user experience -->

    <!-- DataTables -->
    <script src="{{ asset("/bower_components/admin-lte/plugins/datatables/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("/bower_components/admin-lte/plugins/datatables/dataTables.bootstrap.min.js") }}"></script>

    <!-- datetimepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset("/datetimepicker/js/bootstrap-datetimepicker.min.js")}}"></script>
    <script>
    $(function () {
        $('#datetimepicker1').datetimepicker({
            format: 'YYYY/MM/DD hh:mm A'
        });
    });
    $(function () {
        $('#datetimepicker2').datetimepicker({
            format: 'YYYY/MM/DD hh:mm A'
        });
    });
    </script>
    <!-- Select2 -->
    <script src="{{ asset("/bower_components/admin-lte/plugins/select2/select2.full.min.js") }}"></script>
    <script>
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
    });
    </script>

    <!-- Slimscroll -->
    <script src="{{ asset("/bower_components/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js") }}"></script>
    <!-- FastClick -->
    <script src="{{ asset("/bower_components/admin-lte/plugins/fastclick/fastclick.js") }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset("/bower_components/admin-lte/dist/js/app.min.js") }}"></script>

    <script src="{{ asset("/js/post_functions.js") }}"></script>

    <!--Scripts In other Pages-->
    @yield('javascript')

</body>
</html>
