<?php

namespace App\Http\Controllers;

use App\Budget;
use App\Member;
use App\Role;
use App\Classes\BudgetAI;
use App\Classes\RoleCheck;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

use Illuminate\Http\Request;

class BudgetController extends Controller
{
    public function validateBudget(Request $request)
    {
        // Validate fields.
        $this->validate($request, [
            'budget_name' => 'required|max:30',
            'budget_description' => 'required|max:25000',
            'budget_funds' => 'required|numeric|min:100|max:99999999',
            'budget_password' => 'required|min:6|max:25',
            'min_vote' => 'required|integer|min:1|max:100',
            'min_agree_percent' => 'required|integer|min:50|max:100',
            'proposal_deadline' => 'required|date',
            'vote_deadline' => 'required|date'
        ]);

        // Validation success...
        return "valid";
    }

    public function postCreateBudget(Request $request)
    {
        // Call validate function...
        if ($this->validateBudget($request) != "valid")
        {
            return redirect()->back();
        }

        // Get user ID
        $user_id = Auth::user()->id;

        // Generate slug with timestamp.
        $budget_slug = time() . "-" . str_slug($request['budget_name'], "-");

        // If slug already exists, generate new slug with new time.
        // Incase two budgets with same name get created at the same second...
        while (Budget::where('budget_slug', '=', $budget_slug)->count() > 0){
            $budget_slug = time() . "-" . str_slug($request['budget_name'], "-");
        }

        // Make and Save new Budget
        $budget = new Budget();
        $budget->user_id = $user_id;
        $budget->budget_slug = $budget_slug;
        $budget->budget_name = $request['budget_name'];
        $budget->budget_description = clean( $request['budget_description'] );
        $budget->budget_funds = $request['budget_funds'];
        $budget->budget_password = $request['budget_password'];
        $budget->min_vote = $request['min_vote'];
        $budget->min_agree_percent = $request['min_agree_percent'];
        $budget->proposal_deadline = Carbon::parse($request['proposal_deadline']);
        $budget->vote_deadline = Carbon::parse($request['vote_deadline']);
        $budget->save();

        // Make member in Budget.
        $member = new Member();
        $member->budget_id = Budget::where('budget_slug', '=', $budget_slug)->first()->id;
        $member->user_id = $user_id;
        $member->role_id = Role::where('role_name', '=', 'Admin')->first()->id;
        $member->save();

        // Flash success message and redirect to to created budget
        flash('You have successfully created a new budget.', 'success');
        return redirect()->route('budget/{budget_slug}', ['budget_slug' => $budget_slug]);
    }

    public function postEditBudget(Request $request)
    {
        // Call validate function...
        if ($this->validateBudget($request) != "valid")
        {
            return redirect()->back();
        }

        // Get budget ID...
        $budget_id = $request['budget_id'];
        // Make and Save new Budget
        $budget = Budget::find($budget_id);
        $budget->budget_name = $request['budget_name'];
        $budget->budget_description = clean( $request['budget_description'] );
        $budget->budget_funds = $request['budget_funds'];
        $budget->budget_password = $request['budget_password'];
        $budget->min_vote = $request['min_vote'];
        $budget->min_agree_percent = $request['min_agree_percent'];
        $budget->proposal_deadline = Carbon::parse($request['proposal_deadline']);
        $budget->vote_deadline = Carbon::parse($request['vote_deadline']);
        $budget->save();

        // Flash success message and redirect to to created budget
        flash('You have successfully edited the budget.', 'success');
        return redirect()->route('budget/{budget_slug}', ['budget_slug' => $budget->budget_slug]);
    }

    public function getBudget($budget_slug)
    {
        $budget = Budget::where('budget_slug', '=', $budget_slug)->first();
        $isadmin = RoleCheck::isAdmin($budget, Auth::user());
        $priorities = $budget->priorities()->get()->sortByDesc('max_percentage');
        $members = $budget->members()->get();

        return view('budget/budget')->with([
            'budget' => $budget,
            'isadmin' => $isadmin,
            'priorities' => $priorities,
            'members' => $members
        ]);
    }

    public function getCreateBudget(Request $request)
    {
        return view('budget/create_budget');
    }

    public function getBudgetReport($budget_slug)
    {
        $budget = Budget::where('budget_slug', '=', $budget_slug)->first();
        $isadmin = RoleCheck::isAdmin($budget, Auth::user());
        $priorities = $budget->priorities()->get();
        $generated_budget = BudgetAI::generateBudget($budget);
        $finalproposals = $generated_budget['finalproposals'];
        $unused_funds = $generated_budget['unused_funds'];
        $veto_count = $generated_budget['veto_count'];
        $proposal_count = $generated_budget['proposal_count'];

        return view('budget/budget_report')->with([
            'budget' => $budget,
            'isadmin' => $isadmin,
            'priorities' => $priorities,
            'finalproposals' => $finalproposals,
            'unused_funds' => $unused_funds,
            'veto_count' => $veto_count,
            'proposal_count' => $proposal_count,
        ]);
    }
}
