@extends('master')

@section('content')
    <div class="hidden">{{$page_title = 'Help and Support' }}</div>
    <div class='row'>
        <div class='col-md-12'>
            <!-- Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tutorial</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <object data="{{ asset("/tutorial.pdf") }}" type="application/pdf" width="100%" height="800px">
                        <h4>Your browser does not support embedded pdf files.
                            You can download the tutorial
                            <a href="{{ asset("/tutorial.pdf") }}" download>here</a>.
                        </h4>
                    </object>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div><!-- /.col -->

</div><!-- /.row -->
@stop

@section('javascript')

    <script>
    //Insert javascript here
    </script>

@stop
