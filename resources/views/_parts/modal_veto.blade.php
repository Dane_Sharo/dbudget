
<!-- Modal -->
<div class="modal fade" id="addVetoModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Veto</h4>
            </div>

            <!-- Modal Body -->
            <form action="{{ route('veto/create') }}" method="post">
                <div class="modal-body">

                    <input type="hidden" name="budget_id" value="{{ $budget->id }}">
                    <input type="hidden" name="priority_id" value="{{ $priority->id }}">
                    <input type="hidden" name="proposal_id" value="{{ $proposal->id }}">

                    {{-- Display message depending on if the admin has already vetoed or not --}}
                    <div class="hidden">{{$thismember_id =  Auth::user()->members()->where('budget_id', '=', $budget->id)->first()->id}}</div>
                    <div class="hidden">{{$veto = $vetos->where('member_id', '=', $thismember_id)->first()}}</div>
                    @if($vetos->where('member_id', '=', $thismember_id)->count() > 0)
                        <p>You have already vetoed this proposal.</p>
                        <p>You can still edit or cancel your veto.</p>
                    @else
                        <p>Vetoing a proposal blocks it from the final budget.</p>
                    @endif

                    <hr>
                    <div class="form-group">
                        <label for="ProposalName">Veto Reason</label>
                        <input type="text" class="form-control" name="veto_reason" id="veto_reason" placeholder="Justification for veto" value="{{ $veto->veto_reason or Request::old('proposal_name')}}">
                    </div>

                    <div class="btn-group btn-group-justified">
                        @if($vetos->where('member_id', '=', $thismember_id)->count() < 1)
                            <div class="btn-group">
                                <button type="submit" class="btn btn-danger" name="veto_action" value="create">Veto Proposal</button>
                            </div>
                        @else
                            <div class="btn-group">
                                <button type="submit" class="btn btn-primary" name="veto_action" value="edit">Edit Veto</button>
                            </div>
                            <div class="btn-group">
                                <button type="submit" class="btn btn-danger" name="veto_action" value="delete">Remove Veto</button>
                            </div>
                        @endif
                    </div>

                </div>
                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                    <input type="hidden" name="_token" value="{{Session::token()}}">
                </div>
            </form>
        </div>
    </div>
</div>
