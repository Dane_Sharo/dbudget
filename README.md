
## About DBudget

DBudget (Democratic Budget) is a web platform for participatory budgeting that helps facilitate democratic budgeting among small and medium groups.

- Mobile friendly web interface.
- Streamlined proposal development.
- Online collaboration and discussion.
- Automatic creation of budget spending plans using AI agent.

DBudget is designed to be accessible and simple to use. It provides all the necessary tools needed to build and maintain a participatory budgeting project for a small or medium sized group.

## Deploying DBudget

This application runs on a server environment that supports PHP 5.6 and MySQL 5.7 (Other versions may work but are not guaranteed). [Laravel Homestead 5.0](https://laravel.com/docs/5.0/homestead) or [WampServer](http://www.wampserver.com/en/) may be used as an environment to build and test the necessary packages before deployment. It is also recommended to install [Composer](https://getcomposer.org/) and [Bower](https://bower.io/) to ease the deployment of the required packages.

##### Installation

Download and unzip DBudget into the server environment. Next, open up the command prompt or terminal and navigate to the directory where the DBudget files were unzipped. Ensure that the directory contains the 'composer.json' and 'composer.lock' file.

Use composer to install the required package dependencies using the command:

    composer install

Composer will automatically download and install the required packages into the project directory.

Next, [Install AdminLTE](https://almsaeedstudio.com/) in the 'public' directory within the project folder. Within the 'public' directory, install AdminLTE via Bower using the command:

    bower install admin-lte

Note: There may be an issue with the admin-lte package. Navigate to the newly created folder called bower_componets. Ensure that there is a folder inside bower_components named admin-lte. Sometimes the folder may be spelled differently, if so rename it to 'admin-lte'.

##### Setting up the database.

By default, DBudget uses the MySQL database engine. If another type of database besides MySQL is used, appropriate changes have to be made to the /config/database.php file.


Create a MySQL database and a user with all privileges to that database. Change the values in the project .env file to match the settings of the recently created database, user and password.

Once this is done, navigate to the project root and run the database migrations using the artisan command:

    php artisan migrate:refresh --seed

The application is now ready to be used.

## License

At the moment, Danesh Durairetnam retains all copyrights to the DBudget platform. Licensing options may change after further development.
